#include "ch.h"
#include "hal.h"

#include "chrtclib.h"

#include "rtc_event.h"
#include "par.h"

#include "local.h"

/* Clock parameters */

#define RTC_YEAR_ID 0x00050011
#define RTC_MON_ID  0x00050012
#define RTC_DATE_ID 0x00050013
#define RTC_HOUR_ID 0x00050021
#define RTC_MIN_ID  0x00050022
#define RTC_SEC_ID  0x00050023
#define RTC_DAY_ID  0x00050031

BSEMAPHORE_DECL(rtcBSem, FALSE);
#define rtcLock() chBSemWait(&rtcBSem)
#define rtcUnlock() chBSemSignal(&rtcBSem)

static struct tm rtcTime;
static bool_t rtcNeedGet = TRUE;

/*
 * Clock handler
 */

static void updateTimeIsr(RTCDriver *rtcp, rtcevent_t event){
  (void)rtcp;
  (void)event;
  
  chSysLockFromIsr();
  
  rtcNeedGet = TRUE;
  
  chSysUnlockFromIsr();
}

static const RTCHandlerConfig rtch_cfg = {
  &RTCD1,
  updateTimeIsr,
  {
    RTC_EVENT_SECOND,
    RTC_EVENT_END,
  }
};

static RTCHandler rtch_upd;

static msg_t rtcGet(const PARConfig *config, PARCell *val){
  rtcLock();
  
  if(rtcNeedGet){
    rtcGetTimeTm(&RTCD1, &rtcTime);
    
    chSysLock();
    rtcNeedGet = FALSE;
    chSysUnlock();
  }
  
  switch(config->id){
  case RTC_YEAR_ID:
    *((int*)val) = rtcTime.tm_year + 1900;
    break;
  case RTC_MON_ID:
    *((int*)val) = rtcTime.tm_mon + 1;
    break;
  case RTC_DATE_ID:
    *((int*)val) = rtcTime.tm_mday;
    break;
  case RTC_HOUR_ID:
    *((int*)val) = rtcTime.tm_hour;
    break;
  case RTC_MIN_ID:
    *((int*)val) = rtcTime.tm_min;
    break;
  case RTC_SEC_ID:
    *((int*)val) = rtcTime.tm_sec;
    break;
  case RTC_DAY_ID:
    *((int*)val) = rtcTime.tm_wday;
    break;
  }
  
  rtcUnlock();

  return RDY_OK;
}

static msg_t rtcSet(const PARConfig *config, const PARCell *val){
  rtcLock();
  
  if(rtcNeedGet){
    rtcGetTimeTm(&RTCD1, &rtcTime);
    
    chSysLock();
    rtcNeedGet = FALSE;
    chSysUnlock();
  }
  
  switch(config->id){
  case RTC_YEAR_ID:
    rtcTime.tm_year = *((int*)val) - 1900;
    break;
  case RTC_MON_ID:
    rtcTime.tm_mon = *((int*)val) - 1;
    break;
  case RTC_DATE_ID:
    rtcTime.tm_mday = *((int*)val);
    break;
  case RTC_HOUR_ID:
    rtcTime.tm_hour = *((int*)val);
    break;
  case RTC_MIN_ID:
    rtcTime.tm_min = *((int*)val);
    break;
  case RTC_SEC_ID:
    rtcTime.tm_sec = *((int*)val);
    break;
  }
  
  rtcSetTimeTm(&RTCD1, &rtcTime);
  
  rtcUnlock();

  return RDY_OK;
}

/* Year */

static const int rtcDateMin = 1, rtcDateStp = 1;

static const int rtcYearMin = 2000, rtcYearMax = 2100;

static const PARConfig rtcYearCfg = {
  RTC_YEAR_ID, "rtc.year", "&|~;O9 ", NULL,
  "Internal real-time clock year.",
  &PARInt, PARReadable | PARWritable,
  NULL, 0, rtcGet, rtcSet,
  NULL, &rtcYearMin, &rtcYearMax, &rtcDateStp
};

PARObject rtcYear;

/* Mon */

static const int rtcMonMax = 12;

static const PARConfig rtcMonCfg = {
  RTC_MON_ID, "rtc.mon", "&|~;&`~1;EC ", NULL,
  "Internal real-time clock month.",
  &PARInt, PARReadable | PARWritable,
  NULL, 0, rtcGet, rtcSet,
  NULL, &rtcDateMin, &rtcMonMax, &rtcDateStp
};

PARObject rtcMon;

/* Date */

static const int rtcDateMax = 31;

static const PARConfig rtcDateCfg = {
  RTC_DATE_ID, "rtc.date", "9EHb", NULL,
  "Internal real-time clock date.",
  &PARInt, PARReadable | PARWritable,
  NULL, 0, rtcGet, rtcSet,
  NULL, &rtcDateMin, &rtcDateMax, &rtcDateStp
};

PARObject rtcDate;

/* Hour */

static const int rtcTimeMin = 0, rtcTimeStp = 1, rtcTimeMax = 59;

static const int rtcHourMax = 23;

static const PARConfig rtcHourCfg = {
  RTC_HOUR_ID, "rtc.hour", "4AC ", NULL,
  "Internal real-time clock hours.",
  &PARInt, PARReadable | PARWritable,
  NULL, 0, rtcGet, rtcSet,
  NULL, &rtcTimeMin, &rtcHourMax, &rtcTimeStp
};

PARObject rtcHour;

/* Min */

static const PARConfig rtcMinCfg = {
  RTC_MIN_ID, "rtc.min", "&|~;&`~1;UH ", NULL,
  "Internal real-time clock minutes.",
  &PARInt, PARReadable | PARWritable,
  NULL, 0, rtcGet, rtcSet,
  NULL, &rtcTimeMin, &rtcTimeMax, &rtcTimeStp
};

PARObject rtcMin;

/* Sec */

static const PARConfig rtcSecCfg = {
  RTC_SEC_ID, "rtc.sec", "CEK|", NULL,
  "Internal real-time clock seconds.",
  &PARInt, PARReadable | PARWritable,
  NULL, 0, rtcGet, rtcSet,
  NULL, &rtcTimeMin, &rtcTimeMax, &rtcTimeStp
};

PARObject rtcSec;

/* Day of Week */

static const PARConfig rtcDayCfg = {
  RTC_SEC_ID, "rtc.day", "", NULL,
  "Internal real-time clock day of week.",
  &PARInt, PARReadable,
  NULL, 0, rtcGet, NULL,
  NULL, NULL, NULL, NULL
};

PARObject rtcDay;

/* Interface */

void localRtcInit(void){
  parInitObject(&rtcYear);
  parInitObject(&rtcMon);
  parInitObject(&rtcDate);
  parInitObject(&rtcHour);
  parInitObject(&rtcMin);
  parInitObject(&rtcSec);
  parInitObject(&rtcDay);
}

void localRtcStart(void){
  parStart(&rtcDay, &rtcDayCfg);
  parStart(&rtcSec, &rtcSecCfg);
  parStart(&rtcMin, &rtcMinCfg);
  parStart(&rtcHour, &rtcHourCfg);
  parStart(&rtcDate, &rtcDateCfg);
  parStart(&rtcMon, &rtcMonCfg);
  parStart(&rtcYear, &rtcYearCfg);
  
  rtcHandlerStart(&rtch_upd, &rtch_cfg);
}

void localRtcStop(void){
  rtcHandlerStop(&rtch_upd);
  
  parStop(&rtcYear);
  parStop(&rtcMon);
  parStop(&rtcDate);
  parStop(&rtcHour);
  parStop(&rtcMin);
  parStop(&rtcSec);
  parStop(&rtcDay);
}
