#include "ch.h"
#include "hal.h"

#include "string.h"
#include "par.h"

#if PAR_USE_MUTEX || PAR_USE_SEMAPHORE

#if PAR_USE_MUTEX && PAR_USE_SEMAPHORE
#error "Please disable PAR_USE_MUTEX or PAR_USE_SEMAPHORE"
#endif

#if PAR_USE_MUTEX
#define _parLock(par) chMtxLock(&par->mutex)
#define _parUnlock(par) chMtxUnlock()

static MUTEX_DECL(mutex);
#define _parLockPool() chMtxLock(&mutex)
#define _parUnlockPool() chMtxUnlock()
#endif

#if PAR_USE_SEMAPHORE
#define _parLock(par) chBSemWait(&par->bsem)
#define _parUnlock(par) chBSemSignal(&par->bsem)

static BSEMAPHORE_DECL(bsem, FALSE);
#define _parLockPool() chBSemWait(&bsem)
#define _parUnlockPool() chBSemSignal(&bsem)
#endif

#else/*PAR_USE_MUTEX || PAR_USE_SEMAPHORE*/
#warning "Using parameters in multithreading maner may be unsafe! Please enable PAR_USE_MUTEX or PAR_USE_SEMAPHORE"
#define _parLock(par)
#define _parUnlock(par)
#define _parLockPool()
#define _parUnlockPool()
#endif/*PAR_USE_MUTEX || PAR_USE_SEMAPHORE*/

PARObject *pool = NULL;

PARObject *parNext(PARObject *par){
  return par == NULL || pool == NULL ? pool : par->next;
}

PARObject *parPrev(PARObject *par){
  if(pool == NULL || pool == par){
    return NULL;
  }
  PARObject *p = pool;
  
  for(; p != NULL; p = p->next){
    if(p->next == par){
      return p;
    }
  }
  return NULL;
}

size_t parCount(PARMode mode){
  size_t count = 0;
  PARObject *par = NULL;
  
  _parLockPool();
  
  if(mode == PARNothing){
    for(; (par = parNext(par)) != NULL; count++);
  }else{
    for(; (par = parNext(par)) != NULL; ){
      if(parMode(par, mode)){
        count ++;
      }
    }
  }
  
  _parUnlockPool();
  
  return count;
}

#if PAR_USE_NAME
PARObject *parFind(const char *name){
  PARObject *par = NULL;
  
  _parLockPool();
  
  for(; (par = parNext(par)) != NULL; ){
    if(0 == strcmp(par->config->name, name)){
      break;
    }
  }
  
  _parUnlockPool();
  
  return par;
}
#endif

void parInitObject(PARObject *par){
  par->config = NULL;
  par->next = NULL;

#if PAR_USE_MUTEX
  chMtxInit(&par->mutex);
#endif
#if PAR_USE_SEMAPHORE
  chBSemInit(&par->bsem, FALSE);
#endif
}

#if PAR_USE_BACKUP || PAR_USE_FLASH
static msg_t _parSave(PARObject *par);
static msg_t _parLoad(PARObject *par);
#endif

static msg_t _parGet(const PARConfig *config, PARCell *val){
  if(config->get != NULL){
    return config->get(config, val);
  }else if(config->val != NULL){
    memcpy(val, config->val, config->type->size(config));
    return RDY_OK;
  }
  return RDY_RESET;
}

static msg_t _parSet(const PARConfig *config, const PARCell *val){
  msg_t res = RDY_OK;
  
  if(config->set != NULL){
    res = config->set(config, val);
  }

  if(res == RDY_OK){
    if(config->val != NULL){
      memcpy(config->val, val, config->type->size(config));
    }else if(config->set == NULL){
      res = RDY_RESET;
    }
  }
  
  return res;
}

msg_t parRawGet(PARObject *par, PARCell *val){
  msg_t res;
  
  chDbgCheck(par != NULL, "parRawGet");

  _parLock(par);
  
  chDbgAssert(par->config != NULL,
              "parRawGet", "invalid state");
  
  res = _parGet(par->config, val);
  
  _parUnlock(par);

  return res;
}

msg_t parRawSet(PARObject *par, const PARCell *val){
  msg_t res;
  
  chDbgCheck(par != NULL, "parRawSet");

  _parLock(par);
  
  chDbgAssert(par->config != NULL,
              "parRawSet", "invalid state");
  
  res = _parSet(par->config, val);

#if PAR_AUTO_SAVE
  if(res == RDY_OK && parMode(par, PARPersist)){
    _parSave(par);
  }
#endif
  
  _parUnlock(par);

  return res;
}

msg_t parGet(PARObject *par, PARCell *val){
  msg_t res;
  
  chDbgCheck(par != NULL, "parGet");

  _parLock(par);
  
  chDbgAssert(par->config != NULL,
              "parGet", "invalid state");
  
  chDbgAssert(parMode(par, PARReadable),
              "parGet", "invalid operation");
  
  res = _parGet(par->config, val);

  _parUnlock(par);

  return res;
}

msg_t parSet(PARObject *par, const PARCell *val){
  msg_t res;
  
  chDbgCheck(par != NULL, "parSet");

  _parLock(par);
  
  chDbgAssert(par->config != NULL,
              "parSet", "invalid state");

  chDbgAssert(parMode(par, PARWritable),
              "parSet", "invalid operation");
  
  res = _parSet(par->config, val);

#if PAR_AUTO_SAVE
  if(res == RDY_OK && parMode(par, PARPersist)){
    _parSave(par);
  }
#endif
  
  _parUnlock(par);

  return res;
}

msg_t parGetStr(PARObject *par, char *str, size_t len){
  msg_t res;
  
  chDbgCheck(par != NULL, "parGetStr");

  _parLock(par);
  
  chDbgAssert(par->config != NULL,
              "parGetStr", "invalid state");
  
  chDbgAssert(parMode(par, PARReadable),
              "parGetStr", "invalid operation");
  
  res = par->config->type->get(par->config, str, len);
  
  _parUnlock();

  return res;
}

msg_t parSetStr(PARObject *par, const char *str){
  msg_t res;
  
  chDbgCheck(par != NULL, "parSetStr");

  _parLock(par);
  
  chDbgAssert(par->config != NULL,
              "parSetStr", "invalid state");

  chDbgAssert(parMode(par, PARWritable),
              "parSetStr", "invalid operation");
  
  res = par->config->type->set(par->config, str);

#if PAR_AUTO_SAVE
  if(res == RDY_OK && parMode(par, PARPersist)){
    _parSave(par);
  }
#endif
  
  _parUnlock(par);

  return res;
}

#if PAR_USE_BACKUP
#error "Backup persistence not implemented."
#endif

#if PAR_USE_FLASH
#include "flash.h"

#ifndef FLASH_SIZE
#error "FLASH_SIZE undefined"
#endif

#ifndef FLASH_FREE
#error "FLASH_FREE undefined"
#endif

#define FLASH_END ((FLASH_BASE)+(FLASH_SIZE))
#define FLASH_BEGIN ((FLASH_END)-(FLASH_FREE))
#define FLASH_PAGE (1<<10)

#define PAR_NO_ID ((PARId)0xffffffff)

typedef struct {
  PARId id;
  size_t size;
} PARBlock;

uint32_t _parFlashLast = 0;

static msg_t _parSave(PARObject *par){
  if(_parFlashLast == 0){
    for(_parFlashLast = FLASH_BEGIN; _parFlashLast < FLASH_END + sizeof(PARBlock); ){
      const PARBlock *block = (const PARBlock *)flashGet(_parFlashLast);
      
      if(block->id == PAR_NO_ID){
        /* Found first free block */
        break;
      }
      
      _parFlashLast += sizeof(PARBlock) + block->size;
    }
  }
  
  {
    PARBlock block = {
      parId(par),
      parSize(par),
    };
    uint8_t data[parSize(par)];
    
    flashUnlock();
    
    if(_parFlashLast + sizeof(PARBlock) + parSize(par) >= FLASH_END){
      /* Need erase pages */
      for(_parFlashLast = FLASH_BEGIN; _parFlashLast < FLASH_END; _parFlashLast += FLASH_PAGE){
        flashErase(_parFlashLast);
      }
      /* Set to beginning of block */
      _parFlashLast = FLASH_BEGIN;
    }

    /* Write header block */
    flashWrite(_parFlashLast, (uint8_t*)&block, sizeof(PARBlock));
    _parFlashLast += sizeof(PARBlock);
    
    /* Get parameter data */
    _parGet(par->config, data);
    
    /* Write parameter data */
    flashWrite(_parFlashLast, data, parSize(par));
    _parFlashLast += parSize(par);
    
    flashLock();
  }
  
  return RDY_OK;
}

static msg_t _parLoad(PARObject *par){
  uint32_t valAddress = 0;
  const PARBlock *block;
  uint32_t parAddress = FLASH_BEGIN;
  
  for(; parAddress < FLASH_END - sizeof(PARBlock); ){
    /* Get access to block header */
    block = (const PARBlock*)flashGet(parAddress);
    /* Skip parameter block header */
    parAddress += sizeof(PARBlock);
    
    if(block->id == parId(par)){
      /* Save the parameter value address */
      valAddress = parAddress;
    }else if(block->id == PAR_NO_ID){
      /* End of data reached */
      break;
    }
    
    /* Skip parameter value */
    parAddress += block->size;
  }
  
  if(valAddress > 0){
    /* Read the last saved parameter */
    _parSet(par->config, (const PARCell *)flashGet(valAddress));
  }
  
  return valAddress == 0 ? RDY_RESET : RDY_OK;
}

#if 1
msg_t parLoadAll(void){
  PARObject *par = NULL;

  _parLockPool();
  
  for(; (par = parNext(par)) != NULL; ){
    if(parMode(par, PARPersist)){
      parLoad(par);
    }
  }

  _parUnlockPool();
  
  return RDY_OK;
}
#else
msg_t parLoadAll(void){
  /* Count persistent parameters */
  size_t count = parCount(PARPersist);
  
  {
    struct {
      PARObject *par;
      uint32_t address;
    } val[count];

    { /* Get persistent parameters ids */
      size_t i = 0;
      PARObject *par = NULL;
      
      for(; par = parNext(par); ){
        if(parMode(par, PARPersist)){
          val[i++].par = par;
        }
      }
    }
    
    { /* Read parameters values */
      size_t i;
      const PARBlock *block;
      uint32_t parAddress = FLASH_BEGIN;
      
      for(; parAddress < FLASH_END - sizeof(PARBlock); ){
        /* Get access to block header */
        block = (const PARBlock*)flashGet(parAddress);
        /* Skip parameter block header */
        parAddress += sizeof(PARBlock);

        if(block->id == PAR_NO_ID){
          /* End of data reached */
          break;
        }
        
        for(i = 0; i < count; i++){
          if(block->id == parId(val[i].par)){
            /* Save the parameter value address */
            if(block->size == parSize(val[i].par)){
              val[i].address = parAddress;
            }
            break;
          }
        }
        
        /* Skip parameter value */
        parAddress += block->size;
      }
      
      if(_parFlashLast == 0){
        _parFlashLast = parAddress;
      }
    }
  }
  
  return RDY_OK;
}
#endif

#endif/*PAR_USE_FLASH*/

#if PAR_USE_BACKUP || PAR_USE_FLASH

#if PAR_USE_BACKUP && PAR_USE_FLASH
#error "Please disable PAR_USE_FLASH or PAR_USE_BACKUP"
#endif

msg_t parSave(PARObject *par){
  msg_t res;
  
  chDbgCheck(par != NULL, "parSave");

  _parLock(par);
  
  chDbgAssert(par->config != NULL,
              "parSave", "invalid state");

  chDbgAssert(parMode(par, PARPersist),
              "parSave", "invalid operation");
  
  res = _parSave(par);

  _parUnlock(par);
  
  return res;
}

msg_t parLoad(PARObject *par){
  msg_t res;
  
  chDbgCheck(par != NULL, "parLoad");

  _parLock(par);
  
  chDbgAssert(parMode(par, PARPersist),
              "parLoad", "invalid operation");
  
  chDbgAssert(par->config != NULL,
              "parLoad", "invalid state");
  
  res = _parLoad(par);

  _parUnlock(par);
  
  return res;
}

#endif

void parStart(PARObject *par, const PARConfig *config){
  chDbgCheck(par != NULL && config != NULL, "parStart");

  _parLock(par);

  chDbgAssert(!(config->mode & PARReadable) || (config->val != NULL) || (config->get != NULL),
              "parStart", "readability not implemented");
  
  chDbgAssert(!(config->mode & PARWritable) || (config->val != NULL) || (config->set != NULL),
              "parStart", "writability not implemented");
  
  chDbgAssert(!(config->mode & PARPersist) || ((PAR_USE_BACKUP || PAR_USE_FLASH) &&
                                               ((config->val != NULL) || (config->get != NULL &&
                                                                          config->set != NULL))),
              "parStart", "persistence not implemented");
  
  chDbgAssert(par->config == NULL && par->next == NULL,
              "parStart", "invalid state");
  
  par->config = config;

  { /* Add param to queue */
    _parLockPool();
    
    PARObject **parp = &pool;
    
    par->next = *parp;
    *parp = par;

    _parUnlockPool();
  }

#if PAR_AUTO_LOAD
  if(!parMode(par, PARPersist) || _parLoad(par) != RDY_OK){
    /* If not persist or not noaded */
#endif
  if(par->config->def){
    /* Set initial value */
    _parSet(par->config, par->config->def);
  }
#if PAR_AUTO_LOAD
  }
#endif

  _parUnlock(par);
}

void parStop(PARObject *par){
  chDbgCheck(par != NULL, "parStop");

  _parLock(par);
  
  chDbgAssert(par->config != NULL,
              "parStop", "invalid state");
  
  { /* Remove param from queue */
    _parLockPool();
    
    PARObject **parp = &pool;
    
    for(; *parp != NULL; ){
      if(*parp == par){
        *parp = par->next;
        par->next = NULL;
        break;
      }
    }
    
    _parUnlockPool();
  }
  
  par->config = NULL;
  
  _parUnlock(par);
}

#if PAR_USE_INT | PAR_USE_FLOAT
#include "stdlib.h"
#include "chprintf.h"
#endif

#if PAR_USE_INT
static PARSize _parIntValSize(const PARConfig *config){
  (void)config;
  
  return sizeof(int);
}

static msg_t _parIntGetStr(const PARConfig *config, char *str, size_t len){
  msg_t res;
  int val;
  
  res = _parGet(config, (PARCell*)&val);

  if(res == RDY_OK){
    chsnprintf(str, len, "%d", val);
  }

  return RDY_OK;
}

static msg_t _parIntSetStr(const PARConfig *config, const char *str){
  int val = atoi(str);
  
  return _parSet(config, (const PARCell*)&val);
}

const PARType PARInt = {
  "int",
  "Integer number type.",
  _parIntValSize,
  _parIntGetStr,
  _parIntSetStr,
};
#endif

#if PAR_USE_FLOAT
static PARSize _parFloatValSize(const PARConfig *config){
  (void)config;
  
  return sizeof(float);
}

static msg_t _parFloatGetStr(const PARConfig *config, char *str, size_t len){
  msg_t res;
  float val;
  
  res = _parGet(config, (PARCell*)&val);

  if(res == RDY_OK){
    chsnprintf(str, len, "%f", val);
  }

  return res;
}

static msg_t _parFloatSetStr(const PARConfig *config, const char *str){
  float val = atof(str);
  
  return _parSet(config, (const PARCell*)&val);
}

const PARType PARFloat = {
  "float",
  "Floating point number type.",
  _parFloatValSize,
  _parFloatGetStr,
  _parFloatSetStr,
};
#endif

#if PAR_USE_STR
static PARSize _parStrValSize(const PARConfig *config){
  PARSize len = 0;
  
  if(config->val){
    len = strlen((const char*)config->val);
  }
  
  return len;
}

static msg_t _parStrGetStr(const PARConfig *config, char *str, size_t len){
  if(config->val){
    strncpy(str, config->val, len);

    return RDY_OK;
  }

  return RDY_RESET;
}

static msg_t _parStrSetStr(const PARConfig *config, const char *str){
  if(config->val){
    strncpy(config->val, str, config->size);
    
    return RDY_OK;
  }
  
  return RDY_RESET;
}

const PARType PARStr = {
  "str",
  "Null terminated string type.",
  _parStrValSize,
  _parStrGetStr,
  _parStrSetStr,
};
#endif
