#ifndef TRM_USE_SEMAPHORE
#define TRM_USE_SEMAPHORE FALSE
#endif

#ifndef TRM_USE_MAILBOX
#define TRM_USE_MAILBOX FALSE
#endif

#ifndef TRM_USE_CALLBACK
#define TRM_USE_CALLBACK FALSE
#endif

/**
 * @brief The thermistor models.
 */
typedef enum {
  TRMModelBeta,
  TRMModelSHH,
} TRMModel;

/**
 * @brief Generic thermistor model params.
 */
typedef struct {
  TRMModel model;
} TRMParams;

/**
 * @brief The thermistor beta model params.
 */
typedef struct {
  TRMModel model;
  double beta;
  double r0;
  double t0;
} TRMParamsBeta;

/**
 * @brief The thermistor Steinhart-Hart model params.
 */
typedef struct {
  TRMModel model;
  double a;
  double b;
  double c;
} TRMParamsSHH;

/**
 * @brief The temperature units.
 */
typedef enum {
  TRMKelvins,
  TRMCelsius,
  TRMFarenheits,
} TRMUnits;

/**
 * @brief The thermistor measurement options.
 *
 * Convert ADC value to resistance.
 *
 *        Analog VCC
 *
 *            ^
 *            |
 *           .-.
 *           | |
 *        R1 | |
 *           | |
 *           '-'
 *            |
 *        .---o---.
 *        |       |
 *       .-.     .-.
 *       | |     | |
 *    R2 | |  Rt | |
 *       | |     | |
 *       '-'     '-'
 *        |       |
 *        '---o---'
 *            |
 *           _|_
 *
 *        Analog GND
 *
 * If r2 doesn't used, r2 must be set to 0
 */
typedef struct {
  /**
   * @brief The resistance of thermistor.
   */
  uint32_t rt;
  /**
   * @brief The resistance of R1.
   */
  uint32_t r1;
  /**
   * @brief The resistance of R2.
   */
  uint32_t r2;
  /**
   * @brief The resulting temperature units.
   */
  TRMUnits units;
} TRMOptions;

typedef struct TRMDriver TRMDriver;

#if TRM_USE_CALLBACK
typedef void TRMCallbackI(TRMDriver *trm);
#endif

/**
 * @brief The thermistor config.
 */
typedef struct {
  /**
   * @brief The input port.
   */
  ioportid_t port;
  /**
   * @brief The input pad number.
   */
  uint16_t pad;
  /**
   * @brief The thermistor params, wich will be used for conversion.
   */
  const TRMParams *params;
  /**
   * @brief The thermistor options, wich will be used for conversion.
   */
  const TRMOptions *options;
#if TRM_USE_MAILBOX
  /**
   * @brief The mailbox to send update notifications.
   */
  Mailbox *mbox;
  /**
   * @brief The message, which will be sent to mailbox.
   */
  msg_t msg;
#endif
#if TRM_USE_CALLBACK
  TRMCallbackI *cb;
#endif
} TRMConfig;

typedef enum {
  TRMNoFlags = 0x00,
  TRMAvailable = 0x01,
  TRMNeedConvert = 0x02,
} TRMFlags;

/**
 * @brief The thermistor driver.
 */
struct TRMDriver {
  const TRMConfig *config;
#if TRM_USE_SEMAPHORE
  BinarySemaphore bsem;
#endif
  TRMFlags flags;
  adcsample_t sample;
  double value;
};

void trmInitObject(TRMDriver *trm);
void trmStart(TRMDriver *trm, const TRMConfig *config);
void trmStop(TRMDriver *trm);
void trmUpdateI(TRMDriver *trm, adcsample_t value);

msg_t trmGetLast(TRMDriver *trm, double *value);

#if TRM_USE_SEMAPHORE
msg_t trmGetNewTimeout(TRMDriver *trm, double *value, systime_t time);
#define trmGetNew(trm, value) trmGetTimeout(trm, value, TIME_INFINITE)
#endif
