BOARD_SETUP := $(HARDWARE)/board.cfg

protect:
	-openocd -f $(BOARD_SETUP) -c "halt" -c "flash protect 0 0 last on" -c "reset run" -c "shutdown"

unprotect:
	-openocd -f $(BOARD_SETUP) -c "halt" -c "flash protect 0 0 last off" -c "reset run" -c "shutdown"

flash:
	-openocd -f $(BOARD_SETUP) -c "program build/firmware.elf verify reset" -c "shutdown"

debug:
	-openocd -f $(BOARD_SETUP) -c "init" -c "reset init"
