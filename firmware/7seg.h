#ifndef SEG7_USE_SET
#define SEG7_USE_SET FALSE
#endif

#ifndef SEG7_USE_PUT
#define SEG7_USE_PUT TRUE
#endif

#ifndef SEG7_USE_PRINTF
#define SEG7_USE_PRINTF TRUE
#endif

/**
 * @brief The pad config.
 */
typedef struct {
  /**
   * @brief The output port.
   */
  ioportid_t port;
  /**
   * @brief The output pad number.
   */
  uint16_t pad;
} SEG7Output;

/**
 * @brief The segment matrix.
 */
typedef struct {
  SEG7Output a, b, c, d, e, f, g, p;
} SEG7Matrix;

/**
 * @brief The size/length type.
 */
typedef uint8_t SEG7Size;

/**
 * @brief The sign value type.
 */
typedef uint8_t SEG7Value;

/**
 * @brief The segment matrix.
 */
typedef struct {
  SEG7Size matrix;
  SEG7Size column;
} SEG7Sign;

/**
 * @brief The active segment and gate values depending of display type and output logic.
 */
#define SEG7_COMMON_ANODE_SEG FALSE
#define SEG7_COMMON_CATCHODE_SEG TRUE
#define SEG7_COMMON_ANODE_SEG_INV TRUE
#define SEG7_COMMON_CATCHODE_SEG_INV FALSE
#define SEG7_COMMON_ANODE_GATE TRUE
#define SEG7_COMMON_CATCHODE_GATE FALSE
#define SEG7_COMMON_ANODE_GATE_INV FALSE
#define SEG7_COMMON_CATCHODE_GATE_INV TRUE

#define SEG7_RATE_HZ(rate) MS2ST(1000/(rate))
#define SEG7_DUTY_CYCLE(val) ((val)*255/100)

/**
 * @brief The display config.
 *
 * Universal 7-segment display driver allows to use both static and dynamic indication depending from configuration.
 * 
 * Each matrix defines mapping GPIO ports to sign segments (a, b, c, …).
 * Each column defines mapping GPIO port to sign anode/cathode ().
 *
 * If no columns is defined (columns == 0), static indication will be used.
 * If some number of columns is defined (columns > 0), dynamic indication will be used (even if columns == 1).
 * When using static indication, the number of matrixes equals to total number of signs, which you need to display.
 * For dynamic indication the number of matrixes depends from number of signs, which you need to display at the same time.
 */
typedef struct {
  /**
   * @brief The active value of matrix segment.
   */
  bool_t seg_active;
  /**
   * @brief The matrix segment pad mode.
   */
  iomode_t seg_mode;
  /**
   * @brief The active value of column gate.
   */
  bool_t gate_active;
  /**
   * @brief The column gate pad mode.
   */
  iomode_t gate_mode;
  /**
   * @brief Update switch period.
   * 
   * Used for dynamic indication only.
   */
  systime_t period;
  /**
   * @brief Segment highlight duty cycle [0..255].
   * 
   * Used for dynamic indication only.
   * Allows changing brightness of display.
   */
  uint8_t duty_cycle;
  /**
   * @brief The number of matrixes.
   */
  SEG7Size matrixes;
  /**
   * @brief The matrix configs.
   */
  const SEG7Matrix *matrix;
  /**
   * @brief The number of columns.
   */
  SEG7Size columns;
  /**
   * @brief  The column configs.
   */
  const SEG7Output *column;
  /**
   * @brief The number of signs.
   */
  SEG7Size signs;
  /**
   * @brief The sign buffer.
   */
  SEG7Value *buffer;
  /**
   * @brief The sign configs.
   */
  const SEG7Sign sign[];
} SEG7Config;

/**
 * @brief The display state.
 */
typedef enum {
  SEG7_UNINIT = 0,
  SEG7_STOP,
  SEG7_READY,
} SEG7State;

/**
 * @brief The display driver.
 */
typedef struct {
  /**
   * @brief Driver configuration.
   */
  const SEG7Config *config;
  /**
   * @brief Current driver state.
   */
  SEG7State state;
  /**
   * @brief Current active column.
   * 
   * According to dynamic indication algorithm only one column is active at each moment of time.
   */
  uint8_t column;
  /**
   * @brief Update period.
   */
  systime_t period;
  /**
   * @brief Update dead time.
   */
  systime_t deadtime;
  /**
   * @brief Driver update timer.
   */
  VirtualTimer utimer;
} SEG7Driver;

void seg7InitObject(SEG7Driver *seg7);
void seg7Start(SEG7Driver *seg7, const SEG7Config *config);
void seg7Stop(SEG7Driver *seg7);

void seg7DutyCycle(SEG7Driver *seg7, uint8_t duty_cycle);

#if SEG7_USE_SET
void seg7Set(SEG7Driver *seg7, SEG7Size pos, SEG7Size len, const SEG7Value *sign);
#endif

#if SEG7_USE_PUT
void seg7Put(SEG7Driver *seg7, SEG7Size pos, const char *line);
#endif

#if SEG7_USE_PRINTF
void seg7Printf(SEG7Driver *seg7, SEG7Size pos, const char *fmt, ...);
#endif
