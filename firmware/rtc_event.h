#define RTC_EVENT_END ((1<<(8*sizeof(rtcevent_t)))-1)

typedef struct {
  RTCDriver *rtcp;
  rtccb_t cb;
  rtcevent_t event[];
} RTCHandlerConfig;

typedef struct RTCHandler RTCHandler;
struct RTCHandler {
  const RTCHandlerConfig *config;
  RTCHandler *next;
};

void rtcHandlerObjectInit(RTCHandler *handler);
void rtcHandlerStart(RTCHandler *handler, const RTCHandlerConfig *config);
void rtcHandlerStop(RTCHandler *handler);
