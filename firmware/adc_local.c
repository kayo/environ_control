#include "ch.h"
#include "hal.h"

#include "trm.h"

extern TRMDriver TRMD[];

/* Total number of channels to be sampled by a single ADC operation.*/
#define ADC_GRP1_NUM_CHANNELS   7

/* Depth of the conversion buffer, channels are sampled eight times each.*/
#define ADC_GRP1_BUF_DEPTH     1

/*
 * ADC samples buffer.
 */
static adcsample_t samples[ADC_GRP1_NUM_CHANNELS * ADC_GRP1_BUF_DEPTH];

#define ADC_EWMA_BITS 12
#define ADC_EWMA_ONE (1 << ADC_EWMA_BITS)
#define ADC_EWMA_ALPHA (ADC_EWMA_ONE / (ADC_GRP1_BUF_DEPTH + 1))
#define ADC_EWMA_ONE_MINUS_ALPHA (ADC_EWMA_ONE - ADC_EWMA_ALPHA)

/*
 * Exponentially Weighted Moving Average calculation
 */

adcsample_t value[ADC_GRP1_NUM_CHANNELS];

static void adccb(ADCDriver *adcp, adcsample_t *buffer, size_t n) {
  (void)adcp;
  (void)buffer;
  (void)n;

  //adcsample_t *p, *v;
  //size_t i, c;
  size_t c;
  
  chSysLockFromIsr();

  /*
  for(i = 0; i < ADC_GRP1_BUF_DEPTH; i++){
    p = samples + i;
    v = value;
    *v = (uint32_t)*p;
    p += ADC_GRP1_BUF_DEPTH;
    v++;
    for(c = 1; c < ADC_GRP1_NUM_CHANNELS; c++, p += ADC_GRP1_BUF_DEPTH, v++){
      *v = ((uint32_t)*p * ADC_EWMA_ALPHA + (uint32_t)*v * ADC_EWMA_ONE_MINUS_ALPHA) / ADC_EWMA_ONE;
    }
  }
  */
  
  const float Avg_Slope = 4.3;
  const float V_25 = 1.41;
  const float V_ref = 1.2;
  float V_sense = samples[1] / 4096.0 * V_ref;
  float T_sense = (V_25 - V_sense) / Avg_Slope + 25.0;
  
  for(c = 2; c < ADC_GRP1_NUM_CHANNELS; c++){
    trmUpdateI(&TRMD[c-2], samples[c]);
  }
  
  chSysUnlockFromIsr();
}

/*
 * ADC conversion group.
 * Mode:        Linear buffer, 4 samples of 2 channels, SW triggered.
 * Channels:    IN1   (41.5 cycles sample time)
 *              IN2   (41.5 cycles sample time)
 */
static const ADCConversionGroup adcgrpcfg = {
  TRUE,
  ADC_GRP1_NUM_CHANNELS,
  adccb,
  NULL,
  /* HW dependent part.*/
  /*cr1*/ 0,
  /*cr2*/ ADC_CR2_TSVREFE,
  /*smpr1*/ ADC_SMPR1_SMP_SENSOR(ADC_SAMPLE_239P5) | ADC_SMPR1_SMP_VREF(ADC_SAMPLE_239P5), 
  /*smpr2*/ ADC_SMPR2_SMP_AN8(ADC_SAMPLE_239P5) | ADC_SMPR2_SMP_AN3(ADC_SAMPLE_239P5) | ADC_SMPR2_SMP_AN2(ADC_SAMPLE_239P5) | ADC_SMPR2_SMP_AN1(ADC_SAMPLE_239P5) | ADC_SMPR2_SMP_AN0(ADC_SAMPLE_239P5),
  /*sqr1*/ ADC_SQR1_NUM_CH(ADC_GRP1_NUM_CHANNELS),
  /*sqr2*/ ADC_SQR2_SQ7_N(ADC_CHANNEL_IN8),
  /*sqr3*/ ADC_SQR3_SQ6_N(ADC_CHANNEL_IN3) | ADC_SQR3_SQ5_N(ADC_CHANNEL_IN2) | ADC_SQR3_SQ4_N(ADC_CHANNEL_IN1) | ADC_SQR3_SQ3_N(ADC_CHANNEL_IN0) | ADC_SQR3_SQ2_N(ADC_CHANNEL_SENSOR) | ADC_SQR3_SQ1_N(ADC_CHANNEL_VREFINT)
};

void localAdcStart(void){
  adcStart(&ADCD1, NULL);
  
  adcStartConversion(&ADCD1, &adcgrpcfg, samples, ADC_GRP1_BUF_DEPTH);
}

void localAdcStop(void){
  adcStop(&ADCD1);
}
