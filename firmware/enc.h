/**
 * @brief The encoder input configuration.
 */
typedef struct {
  /**
   * @brief The input port.
   */
  ioportid_t port;
  /**
   * @brief The input pad number.
   */
  uint16_t pad;
  /**
   * @brief The port pad mode.
   */
  iomode_t mode;
  /**
   * @brief The active state.
   */
  bool_t state;
  /**
   * @brief The ext driver to use.
   */
  EXTDriver *extp;
  /**
   * @brief The ext channel mode.
   */
  uint32_t extm;
} ENCInput;

/**
 * @brief The encoder driver configuration.
 */
typedef struct {
  /**
   * @brief The input configurations.
   */
  ENCInput a;
  ENCInput b;
  
  /**
   * @brief The mailbox to post encoder events.
   */
  Mailbox *mbox;
  
  /**
   * @brief The messages to send when handler turns.
   */
  msg_t msg_a;
  msg_t msg_b;
} ENCConfig;

typedef struct ENCDriver ENCDriver;

/**
 * @brief The encoder driver object.
 */
struct ENCDriver{
  /**
   * @brief The driver configuration.
   */
  const ENCConfig *config;
  /**
   * @brief The next driver in queue.
   */
  ENCDriver *next;
  /**
   * @brief The sequence of last four phases.
   */
  uint8_t phases;
};

void encInitObject(ENCDriver *enc);
void encStart(ENCDriver *enc, const ENCConfig *config);
void encStop(ENCDriver *enc);
