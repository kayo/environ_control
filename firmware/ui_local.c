#include "ch.h"
#include "hal.h"

#include "chrtclib.h"
//#include "chprintf.h"

#include "par.h"
#include "rtc_event.h"
#include "btn.h"
#include "enc.h"
#include "7seg.h"
#include "fsm_def.h"

/*
 * Encoder as UI control
 */

msg_t ui_mbuf[3];
Mailbox ui_mbox;

#define UI_BUTTON FSM_EVENT(1)
#define UI_PLUS FSM_EVENT(2)
#define UI_MINUS FSM_EVENT(3)
#define UI_RESET FSM_EVENT(4)
#define UI_CLOCK FSM_EVENT(5)
#define UI_AWAY FSM_EVENT(6)

/*
 * Encoder button
 */

const BTNConfig btn_cfg = {
  PORT(ENC_BTN),
  PAD(ENC_BTN),
  MODE(INPUT_PULLUP),
  FALSE,
  &ENC_DRV,
  EXT(ENC_BTN),
  &ui_mbox,
  {
    { UI_BUTTON, MS2ST(100) },
    { UI_RESET, MS2ST(500) },
    BTN_EVENT_END
  }
};

BTNDriver BTND1;

/*
 * Encoder handler
 */

const ENCConfig enc_cfg = {
  {
    PORT(ENC_A),
    PAD(ENC_A),
    MODE(INPUT_PULLUP),
    FALSE,
    &ENC_DRV,
    EXT(ENC_A),
  },
  {
    PORT(ENC_B),
    PAD(ENC_B),
    MODE(INPUT_PULLUP),
    TRUE,
    &ENC_DRV,
    EXT(ENC_B),
  },
  &ui_mbox,
  UI_MINUS,
  UI_PLUS,
};

ENCDriver ENCD1;

/*
 * 7-segment clock display
 */

const SEG7Matrix matrix[] = {
  {
    { PORT(SEG_A), PAD(SEG_A) },
    { PORT(SEG_B), PAD(SEG_B) },
    { PORT(SEG_C), PAD(SEG_C) },
    { PORT(SEG_D), PAD(SEG_D) },
    { PORT(SEG_E), PAD(SEG_E) },
    { PORT(SEG_F), PAD(SEG_F) },
    { PORT(SEG_G), PAD(SEG_G) },
    { PORT(SEG_P), PAD(SEG_P) },
  },
};

const SEG7Output column[] = {
  { PORT(SEG_1), PAD(SEG_1) },
  { PORT(SEG_2), PAD(SEG_2) },
  { PORT(SEG_3), PAD(SEG_3) },
  { PORT(SEG_4), PAD(SEG_4) },
};

SEG7Value buffer[sizeof(column)/sizeof(SEG7Output)];

const SEG7Config seg7_cfg = {
  SEG7_COMMON_CATCHODE_SEG,
  MODE(OUTPUT_PUSHPULL),
  SEG7_COMMON_CATCHODE_GATE_INV,
  MODE(OUTPUT_PUSHPULL),
  SEG7_RATE_HZ(60),
  SEG7_DUTY_CYCLE(50),
  sizeof(matrix)/sizeof(SEG7Matrix),
  matrix,
  sizeof(column)/sizeof(SEG7Output),
  column,
  sizeof(buffer)/sizeof(SEG7Value),
  buffer,
  {
    { 0, 0 },
    { 0, 1 },
    { 0, 2 },
    { 0, 3 },
  }
};

SEG7Driver SEG7D1;

/*
 * Clock handler
 */

static void sendClock(RTCDriver *rtcp, rtcevent_t event){
  (void)rtcp;
  (void)event;
  
  chSysLockFromIsr();
  
  chMBPostI(&ui_mbox, UI_CLOCK);
  
  chSysUnlockFromIsr();
}

static const RTCHandlerConfig rtch_clock_cfg = {
  &RTCD1,
  sendClock,
  {
    RTC_EVENT_SECOND,
    RTC_EVENT_END,
  }
};

static RTCHandler rtch_clock;

/*
 * User Interface States
 */

FSM_STATE(ui_display);
FSM_STATE(ui_param_view_select);
FSM_STATE(ui_param_view_display);
FSM_STATE(ui_param_edit_select);
FSM_STATE(ui_param_edit_change);

/*
 * UI State Machine
 */

FSM_STATE(ui_display){
  FSM_ACTION(FSM_ENTER){ rtcHandlerStart(&rtch_clock, &rtch_clock_cfg); }
  FSM_ACTION(FSM_LEAVE){ rtcHandlerStop(&rtch_clock); }
  //FSM_ACTION(UI_BUTTON){ }
  FSM_ACTION(UI_RESET){ FSM_SWITCH(ui_param_edit_select); }
  FSM_ACTION(UI_PLUS){ FSM_SWITCH(ui_param_view_select); }
  FSM_ACTION(UI_MINUS){ FSM_SWITCH(ui_param_view_select); }
  FSM_ACTION(UI_CLOCK){
    enum {
      SHOW_YEAR,
      SHOW_DATE,
      SHOW_TIME,
      SHOW_TICK,
      SHOW_LAST,
    };
    
    static int count = 0;
    struct tm time;
    
    rtcGetTimeTm(&RTCD1, &time);
    
    switch(count >> 2){
    case SHOW_YEAR:
      seg7Printf(&SEG7D1, 0, "%04u", time.tm_year + 1900);
      break;
    case SHOW_DATE:
      seg7Printf(&SEG7D1, 0, "%02u\b.\f%02u", time.tm_mday, time.tm_mon + 1);
      break;
    case SHOW_TIME:
      seg7Printf(&SEG7D1, 0, "%02u%s%02u", time.tm_hour, count & 0b1 ? "." : "", time.tm_min);
      break;
    case SHOW_TICK:
      seg7Printf(&SEG7D1, 0, "  %s%02u", count & 0b1 ? "." : "", time.tm_sec);
      break;
    }
    
    count++;
    
    if(count >> 2 >= SHOW_LAST){
      count = 0;
    }
  }
}

static PARObject *par = NULL;

static void selectParam(int n, PARMode mask, PARMode bits){
  if(n < 0){
    for(; n < 0; ){
      if((par = parPrev(par)) != NULL &&
         parMode2(par, mask, bits)){
        /* Iterate over matched parameters only */
        n++;
      }
    }
    
    if(par == NULL){
      for(; (par = parPrev(par)) != NULL; ){
        if(parMode2(par, mask, bits)){
          break;
        }
      }
    }
  }else{
    if(n == 0){
      /* Reset par */
      if(par != NULL && !parMode2(par, mask, bits)){
        par = NULL;
      }
    }
    
    for(; n > 0; ){
      if((par = parNext(par)) != NULL &&
         parMode2(par, mask, bits)){
        /* Iterate over matched parameters only */
        n--;
      }
    }
    
    if(par == NULL){
      for(; (par = parNext(par)) != NULL; ){
        if(parMode2(par, mask, bits)){
          break;
        }
      }
    }
  }
  
  if(par){
    seg7Printf(&SEG7D1, 0, parHint(par));
  }
}

static void sendAway(void *arg){
  (void)arg;

  chSysLockFromIsr();
  
  chMBPostI(&ui_mbox, UI_AWAY);
  
  chSysUnlockFromIsr();
}

VirtualTimer awayVT;

static void awayStop(void){
  chVTReset(&awayVT);
}

static void awayRestart(void){
  chVTReset(&awayVT);
  chVTSet(&awayVT, S2ST(15), sendAway, NULL);
}

FSM_STATE(ui_param_view_select){
  FSM_ACTION(FSM_ENTER){ selectParam(0, PARAccess, PARReadable); awayRestart(); }
  FSM_ACTION(FSM_LEAVE){ awayStop(); }
  FSM_ACTION(UI_PLUS){ selectParam(1, PARAccess, PARReadable); awayRestart(); }
  FSM_ACTION(UI_MINUS){ selectParam(-1, PARAccess, PARReadable); awayRestart(); }
  FSM_ACTION(UI_BUTTON){ FSM_SWITCH(ui_param_view_display); }
  FSM_ACTION(UI_RESET){ FSM_SWITCH(ui_display); }
  FSM_ACTION(UI_AWAY){ FSM_SWITCH(ui_display); }
}

FSM_STATE(ui_param_edit_select){
  FSM_ACTION(FSM_ENTER){ selectParam(0, PARWritable, PARWritable); }
  FSM_ACTION(UI_PLUS){ selectParam(1, PARWritable, PARWritable); }
  FSM_ACTION(UI_MINUS){ selectParam(-1, PARWritable, PARWritable); }
  FSM_ACTION(UI_BUTTON){ if(par) FSM_SWITCH(ui_param_edit_change); }
  FSM_ACTION(UI_RESET){ FSM_SWITCH(ui_display); }
}

static bool_t valid = FALSE;
static int intVal;

static void changeParam(int n){
  if(parType(par, &PARInt)){
    if(n == 0){
      valid = RDY_OK == parGet(par, &intVal);
    }else{
      intVal += n * parCons(par, stp, int);
      
      if(intVal > parCons(par, max, int)){
        intVal = parCons(par, max, int);
      }
      
      if(intVal < parCons(par, min, int)){
        intVal = parCons(par, min, int);
      }
    }

    if(valid){
      seg7Printf(&SEG7D1, 0, parUnit(par) ? parUnit(par) : "%4d", intVal);
    }else{
      seg7Printf(&SEG7D1, 0, "----");
    }
  }
}

FSM_STATE(ui_param_view_display){
  FSM_ACTION(FSM_ENTER){ changeParam(0); rtcHandlerStart(&rtch_clock, &rtch_clock_cfg); }
  FSM_ACTION(FSM_LEAVE){ rtcHandlerStop(&rtch_clock); }
  FSM_ACTION(UI_CLOCK){ changeParam(0); }
  FSM_ACTION(UI_PLUS){ FSM_SWITCH(ui_param_view_select); }
  FSM_ACTION(UI_MINUS){ FSM_SWITCH(ui_param_view_select); }
  FSM_ACTION(UI_BUTTON){ FSM_SWITCH(ui_param_view_select); }
  FSM_ACTION(UI_RESET){ FSM_SWITCH(ui_param_view_select); }
}

static void resetParam(void){
  if(parType(par, &PARInt)){
    intVal = parCons(par, def, int);
    
    seg7Printf(&SEG7D1, 0, "%4d", intVal);
  }
}

static void applyParam(void){
  if(parType(par, &PARInt)){
    parSet(par, &intVal);
  }
}

FSM_STATE(ui_param_edit_change){
  FSM_ACTION(FSM_ENTER){ changeParam(0); }
  FSM_ACTION(UI_PLUS){ changeParam(1); }
  FSM_ACTION(UI_MINUS){ changeParam(-1); }
  FSM_ACTION(UI_RESET){ resetParam(); }
  FSM_ACTION(UI_BUTTON){ applyParam(); FSM_SWITCH(ui_param_edit_select); }
}

static WORKING_AREA(ui_thread_wa, 512);
static msg_t ui_thread_fn(void *arg){
  (void)arg;
  
  msg_t msg;
  
  /* Define machine with name `ui` and initial state `init` */
  FSM_DEFINE(ui, ui_display);
  
  chRegSetThreadName("ui");
  
  /* Start `ui` machine */
  FSM_START(ui);
  
  for(; chMBFetch(&ui_mbox, &msg, TIME_INFINITE) == RDY_OK; ){
    FSM_HANDLE(ui, msg);
  }

  /* Start `ui` machine */
  FSM_STOP(ui);
  
  return 0;
}

/*
 * Interface
 */

void localUiInit(void){
  rtcHandlerObjectInit(&rtch_clock);
  
  btnInitObject(&BTND1);
  
  encInitObject(&ENCD1);
  
  seg7InitObject(&SEG7D1);
}

void localUiStart(void){
  seg7Start(&SEG7D1, &seg7_cfg);
  
  seg7Put(&SEG7D1, 0, "8.8.88");
  
  chMBInit(&ui_mbox, ui_mbuf, sizeof(ui_mbuf)/sizeof(msg_t));
  
  btnStart(&BTND1, &btn_cfg);
  encStart(&ENCD1, &enc_cfg);
  
  chThdCreateStatic(ui_thread_wa, sizeof(ui_thread_wa), NORMALPRIO, ui_thread_fn, NULL);
}

void localUiStop(void){
  btnStop(&BTND1);
  encStop(&ENCD1);
  
  chMBReset(&ui_mbox);
  
  seg7Stop(&SEG7D1);
}
