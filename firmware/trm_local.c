#include "ch.h"
#include "hal.h"

#include "trm.h"
#include "par.h"

#include "local.h"

#define TRM_PAR_ID(n) (0x00030000 | (n))
#define TRM_PAR_NUM(id) (~0x00030000 & (id))

static const TRMParamsBeta trm_beta = {
  TRMModelBeta,
  2817.7107349079056,
  100000,
  297.15,
};

static const TRMParamsSHH trm_shh = {
  TRMModelSHH,
  0.0013081992342927878,
  0.00005479611105636211,
  9.415113523239857e-7,
};

static const TRMOptions trm_std = {
  10000,
  4700,
  0,
  TRMCelsius,
};

static const TRMConfig trm_cfg[] = {
#ifdef T1_IN
  {
    PORT(T1_IN), PAD(T1_IN),
    (TRMParams*)&trm_shh, &trm_std,
  },
#endif
#ifdef T2_IN
  {
    PORT(T2_IN), PAD(T2_IN),
    (TRMParams*)&trm_shh, &trm_std,
  },
#endif
#ifdef T3_IN
  {
    PORT(T3_IN), PAD(T3_IN),
    (TRMParams*)&trm_shh, &trm_std,
  },
#endif
#ifdef T4_IN
  {
    PORT(T4_IN), PAD(T4_IN),
    (TRMParams*)&trm_shh, &trm_std,
  },
#endif
#ifdef TS_IN
  {
    PORT(TS_IN), PAD(TS_IN),
    (TRMParams*)&trm_shh, &trm_std,
  },
#endif
};

#define TRM_COUNT (sizeof(trm_cfg)/sizeof(trm_cfg[0]))

TRMDriver TRMD[TRM_COUNT];

static msg_t trmGet(const PARConfig *config, PARCell *val){
  double t;
  
  if(RDY_OK == trmGetLast(&TRMD[TRM_PAR_NUM(config->id)], &t)){
    if(t < -100 || t > 200){
      /* validation */
      return RDY_TIMEOUT;
    }
    
    *((int*)val) = t;
    
    return RDY_OK;
  }
  
  return RDY_RESET;
}

#define celsius "%2d*C"

static const PARConfig trmCfg[] = {
#ifdef T1_IN
  {
    TRM_PAR_ID(0), "air.t.1", "tA1*", celsius,
    "Temperature sensor 1.",
    &PARInt, PARReadable,
    NULL, 0, trmGet, NULL,
    NULL, NULL, NULL, NULL
  },
#endif
#ifdef T2_IN
  {
    TRM_PAR_ID(1), "air.t.2", "tA2*", celsius,
    "Temperature sensor 2.",
    &PARInt, PARReadable,
    NULL, 0, trmGet, NULL,
    NULL, NULL, NULL, NULL
  },
#endif
#ifdef T3_IN
  {
    TRM_PAR_ID(2), "oil.t.1", "tO1*", celsius,
    "Temperature sensor 3.",
    &PARInt, PARReadable,
    NULL, 0, trmGet, NULL,
    NULL, NULL, NULL, NULL
  },
#endif
#ifdef T4_IN
  {
    TRM_PAR_ID(3), "oil.t.2", "tO2*", celsius,
    "Temperature sensor 4.",
    &PARInt, PARReadable,
    NULL, 0, trmGet, NULL,
    NULL, NULL, NULL, NULL
  },
#endif
#ifdef TS_IN
  {
    TRM_PAR_ID(4), "psw.t", "tS *", celsius,
    "Temperature sensor s.",
    &PARInt, PARReadable,
    NULL, 0, trmGet, NULL,
    NULL, NULL, NULL, NULL
  },
#endif
};

PARObject trmPar[TRM_COUNT];

void localTrmInit(void){
  uint8_t i;

  for(i = 0; i < TRM_COUNT; i++){
    trmInitObject(&TRMD[i]);
    parInitObject(&trmPar[i]);
  }
}

void localTrmStart(void){
  uint8_t i;

  for(i = 0; i < TRM_COUNT; i++){
    trmStart(&TRMD[i], &trm_cfg[i]);
    parStart(&trmPar[i], &trmCfg[i]);
  }
}

void localTrmStop(void){
  uint8_t i;
  
  for(i = 0; i < TRM_COUNT; i++){
    parStop(&trmPar[i]);
    trmStop(&TRMD[i]);
  }
}
