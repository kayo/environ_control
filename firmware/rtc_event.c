#include "ch.h"
#include "hal.h"

#include "rtc_event.h"

#if RTC_SUPPORTS_CALLBACKS

RTCHandler *handlers = NULL;

static bool_t _rtcHandlerMatch(const rtcevent_t *eventp, rtcevent_t event){
  for(; *eventp != RTC_EVENT_END; eventp++){
    if(*eventp == event){
      return TRUE;
    }
  }
  return FALSE;
}

static void _rtcHandlerIsr(RTCDriver *rtcp, rtcevent_t event){
  RTCHandler *handler = handlers;
  
  for(; handler; handler = handler->next){
    const RTCHandlerConfig *config = handler->config;
    if(config->rtcp == rtcp && _rtcHandlerMatch(config->event, event)){
      config->cb(rtcp, event);
    }
  }
}

void rtcHandlerObjectInit(RTCHandler *handler){
  handler->config = NULL;
  handler->next = NULL;
}

void rtcHandlerStart(RTCHandler *handler, const RTCHandlerConfig *config){
  chDbgCheck(handler != NULL &&
             config != NULL &&
             config->rtcp != NULL &&
             config->cb != NULL,
             "rtcHandlerStart");
  
  chSysLock();
  
  chDbgAssert(handler->config == NULL &&
              handler->next == NULL,
              "rtcHandlerStart()", "invalid state");

  /* Set config */
  handler->config = config;
  
  if(handlers == NULL){
    /* Set global RTC callback */
    rtcSetCallbackI(config->rtcp, _rtcHandlerIsr);
  }
  
  RTCHandler **handlerp = &handlers;
  
  /* Add new handler to queue */
  handler->next = *handlerp;
  *handlerp = handler;
  
  chSysUnlock();
}

void rtcHandlerStop(RTCHandler *handler){
  chDbgCheck(handler != NULL,
             "rtcHandlerStop");
  
  chSysLock();
  
  chDbgAssert(handler->config != NULL,
              "rtcHandlerStop()", "invalid state");

  RTCHandler **handlerp = &handlers;
  
  /* Find pointer to handler */
  for(; *handlerp != NULL && *handlerp != handler; handlerp = &(*handlerp)->next);
  
  chDbgAssert(*handlerp != NULL,
              "rtcHandlerStop()", "invalid state");
  
  /* Switch pointer to next */
  *handlerp = handler->next;
  /* Clear next */
  handler->next = NULL;
  
  if(handlers == NULL){
    /* Clear global RTC callback */
    rtcSetCallbackI(handler->config->rtcp, NULL);
  }
  
  /* Clear config */
  handler->config = NULL;
  
  chSysUnlock();
}

#endif
