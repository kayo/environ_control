#include "ch.h"
#include "hal.h"

#include "btn.h"

void btnInitObject(BTNDriver *btn){
  btn->config = NULL;
}

static BTNDriver *pool = NULL;

static BTNDriver *_btnFind(EXTDriver *extp, expchannel_t channel){
  BTNDriver *btn = pool;
  
  for(; btn != NULL; btn = btn->next){
    if(btn->config->extp == extp &&
       btn->config->pad == channel){
      return btn;
    }
  }
  
  return NULL;
}

static void _btnHandleIsr(EXTDriver *extp, expchannel_t channel){
  (void)extp;
  
  chSysLockFromIsr();
  
  systime_t time = chTimeNow();
  BTNDriver *btn = _btnFind(extp, channel);
  
  bool_t state = !btn->config->state ^ palReadPad(btn->config->port, btn->config->pad);
  systime_t delay = time - btn->last_time;
  const BTNEvent *event = btn->config->poll;
  
  if(state != btn->last_state && event->time > 0 && delay > event->time){
    btn->last_state = state;
    btn->last_time = time;
    
    if(!state){
      /* Find event with maximum delay time */
      for(; event->time > 0 && delay > event->time; ++event);
      --event;
      
      if(btn->config->mbox){
        /* Send event message */
        chMBPostI(btn->config->mbox, event->msg);
      }
    }
  }
  
  chSysUnlockFromIsr();
}

void btnStart(BTNDriver *btn, const BTNConfig *config){
  chDbgCheck(btn != NULL && config != NULL, "btnStart");
  
  chSysLock();
  
  chDbgAssert(btn->config == NULL,
              "btnStart()", "invalid state");
  
  btn->config = config;
  
  { /* Add button to queue */
    BTNDriver **btnp = &pool;
    
    btn->next = *btnp;
    *btnp = btn;
  }
  
  /* Configure port */
  palSetPadMode(btn->config->port, btn->config->pad, btn->config->mode);
  
  /* Determing initial button state */
  btn->last_state = !btn->config->state ^ palReadPad(btn->config->port, btn->config->pad);
  
  { /* Configure ext channel */
    const EXTChannelConfig ch = {
      btn->config->extm | EXT_CH_MODE_BOTH_EDGES,
      _btnHandleIsr,
    };
    
    /* Turn on ext channel */
    extSetChannelModeI(btn->config->extp, btn->config->pad, &ch);
  }
  
  chSysUnlock();
}

void btnStop(BTNDriver *btn){
  chDbgCheck(btn != NULL, "btnStop");
  
  chSysLock();
  
  chDbgAssert(btn->config != NULL,
              "btnStop()", "invalid state");
  
  /* Turn off ext channel */
  extChannelDisable(btn->config->extp, btn->config->pad);
  
  /* Deconfigure port */
  palSetPadMode(btn->config->port, btn->config->pad, PAL_MODE_UNCONNECTED);
  
  { /* Remove button from queue */
    BTNDriver **btnp = &pool;
    
    for(; *btnp != NULL; ){
      if(*btnp == btn){
        *btnp = btn->next;
        btn->next = NULL;
        break;
      }
    }
  }
  
  btn->config = NULL;
  
  chSysUnlock();
}
