#ifndef PAR_USE_MUTEX
#define PAR_USE_MUTEX TRUE
#endif

#ifndef PAR_USE_SEMAPHORE
#define PAR_USE_SEMAPHORE FALSE
#endif

#ifndef PAR_USE_FLASH
#define PAR_USE_FLASH TRUE
#endif

#ifndef PAR_USE_BACKUP
#define PAR_USE_BACKUP FALSE
#endif

#ifndef PAR_AUTO_LOAD
#define PAR_AUTO_LOAD TRUE
#endif

#ifndef PAR_AUTO_SAVE
#define PAR_AUTO_SAVE TRUE
#endif

#ifndef PAR_USE_ID
#define PAR_USE_ID TRUE
#endif

#ifndef PAR_USE_NAME
#define PAR_USE_NAME TRUE
#endif

#ifndef PAR_USE_HINT
#define PAR_USE_HINT TRUE
#endif

#ifndef PAR_USE_UNIT
#define PAR_USE_UNIT TRUE
#endif

#ifndef PAR_USE_DESC
#define PAR_USE_DESC TRUE
#endif

#ifndef PAR_USE_INT
#define PAR_USE_INT TRUE
#endif

#ifndef PAR_USE_FLOAT
#define PAR_USE_FLOAT TRUE
#endif

#ifndef PAR_USE_STR
#define PAR_USE_STR TRUE
#endif

typedef uint32_t PARId;
typedef size_t PARSize;
typedef void PARCell;
typedef struct PARConfig PARConfig;

typedef PARSize PARValSize(const PARConfig *config);
typedef msg_t PARSetStr(const PARConfig *config, const char *str);
typedef msg_t PARGetStr(const PARConfig *config, char *str, size_t len);

/**
 * @brief The type of parameter.
 */
typedef struct {
  /**
   * @brief Parameter type name.
   */
  const char *name;
  /**
   * @brief Parameter type description.
   */
  const char *desc;
  /**
   * @brief Parameter value size getter.
   */
  PARValSize *size;
  /**
   * @brief Value to string converter.
   */
  PARGetStr *get;
  /**
   * @brief String to value converter.
   */
  PARSetStr *set;
} PARType;

#if PAR_USE_INT
extern const PARType PARInt;
#endif

#if PAR_USE_FLOAT
extern const PARType PARFloat;
#endif

#if PAR_USE_STR
extern const PARType PARStr;
#endif

/**
 * @brief The access mode of parameter.
 */
typedef enum {
  PARNothing  = 0x0,
  PARReadable = 0x1,
  PARWritable = 0x2,
  PARAccess   = 0x3, /**< Access bits mask */
  PARPersist  = 0x4, /**< Persistent value (save/load) */
} PARMode;

typedef msg_t PARGet(const PARConfig *config, PARCell *cell);
typedef msg_t PARSet(const PARConfig *config, const PARCell *cell);

/**
 * @brief The config of parameter.
 */
struct PARConfig {
#if PAR_USE_ID
  /**
   * @brief Parameter unique id.
   */
  PARId id;
#endif
#if PAR_USE_NAME
  /**
   * @brief Parameter name.
   */
  const char *name;
#endif
#if PAR_USE_HINT
  /**
   * @brief Parameter hint for menu.
   */
  const char *hint;
#endif
#if PAR_USE_UNIT
  /**
   * @brief Parameter unit for menu.
   */
  const char *unit;
#endif
#if PAR_USE_DESC
  /**
   * @brief Parameter description.
   */
  const char *desc;
#endif
  /**
   * @brief Parameter type.
   */
  const PARType *type;
  /**
   * @brief Parameter access mode.
   */
  PARMode mode;
  /**
   * @brief Parameter value cell.
   *
   * NULL means that getter/setter will be used.
   */
  PARCell *val;
  /**
   * @brief Size of value cell.
   *
   * Means maximum number of bytes, which can be saved to value cell.
   */
  PARSize size;
  /**
   * @brief Parameter value getter.
   */
  PARGet *get;
  /**
   * @brief Parameter value setter.
   */
  PARSet *set;
  /**
   * @brief Default parameter value.
   */
  const PARCell *def;
  /**
   * @brief Minimum parameter value.
   */
  const PARCell *min;
  /**
   * @brief Maximum parameter value.
   */
  const PARCell *max;
  /**
   * @brief Parameter change step.
   */
  const PARCell *stp;
};

/**
 * @brief Parameter object.
 */
typedef struct PARObject PARObject;
struct PARObject {
  const PARConfig *config;
  PARObject *next;
#if PAR_USE_MUTEX
  Mutex mutex;
#endif
#if PAR_USE_SEMAPHORE
  BinarySemaphore bsem;
#endif
};

void parInitObject(PARObject *par);
void parStart(PARObject *par, const PARConfig *config);
void parStop(PARObject *par);

PARObject *parNext(PARObject *par);
PARObject *parPrev(PARObject *par);

/**
 * @brief Counts parameters, which has required mode bits.
 *
 * Use PARNothing to count all parameters.
 */
size_t parCount(PARMode mode);

#if PAR_USE_NAME
PARObject *parFind(const char *name);
#endif

msg_t parRawGet(PARObject *par, PARCell *val);
msg_t parRawSet(PARObject *par, const PARCell *val);

msg_t parGet(PARObject *par, PARCell *val);
msg_t parSet(PARObject *par, const PARCell *val);

msg_t parGetStr(PARObject *par, char *str, size_t len);
msg_t parSetStr(PARObject *par, const char *str);

#if PAR_USE_FLASH | PAR_USE_BACKUP
msg_t parSave(PARObject *par);
msg_t parLoad(PARObject *par);
msg_t parLoadAll(void);
#endif

#if PAR_USE_ID
#define parId(par) ((par)->config->id)
#endif

#if PAR_USE_NAME
#define parName(par) ((par)->config->name)
#endif

#if PAR_USE_HINT
#define parHint(par) ((par)->config->hint)
#endif

#if PAR_USE_UNIT
#define parUnit(par) ((par)->config->unit)
#endif

#if PAR_USE_DESC
#define parDesc(par) ((par)->config->desc)
#endif

#define parSize(par) ((par)->config->type->size((par)->config))
#define parCons(par, name, type) (*(type*)((par)->config->name))

#define parType(par, ptr) ((par)->config->type == (ptr))
#define parMode(par, bits) ((par)->config->mode & (bits))
#define parMode2(par, mask, bits) (((par)->config->mode & (mask)) == (bits))
