/**
 * @brief The input pad configuration.
 *
 * To reduce noise switching of the high power load may be sinchronized with zero cross moments.
 * Input can be used for zero cross detection.
 */
typedef struct {
  /**
   * @brief The port.
   */
  ioportid_t port;
  /**
   * @brief The pad number.
   */
  uint16_t pad;
  /**
   * @brief The port pad mode.
   */
  iomode_t mode;
  /**
   * @brief The active state.
   */
  bool_t state;

  /**
   * @brief The ext driver to use.
   */
  EXTDriver *extp;
  /**
   * @brief The ext channel mode.
   */
  uint32_t extm;
} PSWInput;

/**
 * @brief The output pad configuration.
 *
 * Each pad can be user to switch single load.
 */
typedef struct {
  /**
   * @brief The port.
   */
  ioportid_t port;
  /**
   * @brief The pad number.
   */
  uint16_t pad;
  /**
   * @brief The port pad mode.
   */
  iomode_t mode;
  /**
   * @brief The active state.
   */
  bool_t state;
} PSWOutput;

/**
 * @brief The power switch configuration.
 */
typedef struct {
  const PSWInput *input;
  size_t outputs;
  const PSWOutput output[];
} PSWConfig;

typedef struct PSWDriver PSWDriver;

/**
 * @brief The power switch driver.
 */
struct PSWDriver {
  const PSWConfig *config;
  /**
   * @brief The duty value.
   */
  size_t value;
  PSWDriver *next;
};

void pswInitObject(PSWDriver *psw);
void pswStart(PSWDriver *psw, const PSWConfig *config);
void pswStop(PSWDriver *psw);

/**
 * @brief Set current state of output.
 */
void pswSet(PSWDriver *psw, size_t value);
