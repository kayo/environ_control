#ifndef FLASH_USE_MUTEX
#define FLASH_USE_MUTEX TRUE
#endif

#ifndef FLASH_USE_SEMAPHORE
#define FLASH_USE_SEMAPHORE FALSE
#endif

#ifndef FLASH_USE_INTERRUPT
#define FLASH_USE_INTERRUPT TRUE
#endif

#ifndef FLASH_USE_EOP
#define FLASH_USE_EOP FALSE
#endif

#if defined(STM32F10X_HD) || defined(STM32F10X_CL) || defined(STM32F10X_HD_VL)
#define FLASH_BLOCK_SIZE (2<<10) /* 2 Kb */
#elif defined(STM32F10X_MD) || defined(STM32F10X_LD) || defined(STM32F10X_MD_VL) || defined(STM32F10X_LD_VL)
#define FLASH_BLOCK_SIZE (1<<10) /* 1 Kb */
#endif

/**
 * @brief Flash status
 */
typedef enum {
  FlashSuccess   = 0x0, /**< Flash operation success */
  FlashNotEmpty  = 0x1, /**< Destination isn't empty */
  FlashProtected = 0x2, /**< Destination protected */
  FlashInvalid   = 0x4, /**< Verification error */
} FlashStatus;

void flashLock(void);
void flashUnlock(void);

FlashStatus flashErase(uint32_t address);
FlashStatus flashWrite(uint32_t address, const uint8_t* data, size_t size);
FlashStatus flashCheck(uint32_t address, const uint8_t* data, size_t size);

#define flashGet(address) ((__IO uint8_t*)(address))

static inline uint8_t flashRead(uint32_t address){
  return (*(__IO uint8_t*)address);
}
