#include "ch.h"
#include "hal.h"

/*
 * PWM configuration structure.
 */
static PWMConfig pwmcfg = {
  1e5,                                  /* 100KHz PWM clock frequency.   */
  10e3,                                 /* PWM period 10mS (in ticks).    */
  NULL,
  {
#if FAN_CH == 1
    {FAN_POL, NULL},
#else
    {PWM_OUTPUT_DISABLED, NULL},
#endif
#if FAN_CH == 2
    {FAN_POL, NULL},
#else
    {PWM_OUTPUT_DISABLED, NULL},
#endif
#if FAN_CH == 3
    {FAN_POL, NULL},
#else
    {PWM_OUTPUT_DISABLED, NULL},
#endif
#if FAN_CH == 4
    {FAN_POL, NULL},
#else
    {PWM_OUTPUT_DISABLED, NULL},
#endif
  },
  /* HW dependent part.*/
  0,
  0,
#if STM32_PWM_USE_ADVANCED
  0
#endif
};

void localPwmStart(void){
  pwmStart(&FAN_DRV, &pwmcfg);

  palSetPadMode(PORT(FAN_OUT), PAD(FAN_OUT), PAL_MODE_STM32_ALTERNATE_PUSHPULL);
}

void localPwmStop(void){
  palSetPadMode(PORT(FAN_OUT), PAD(FAN_OUT), PAL_MODE_UNCONNECTED);
  
  pwmStop(&FAN_DRV);
}

void fanSet(float val){
  if(val > 1e-6){
    pwmEnableChannel(&FAN_DRV, FAN_CH-1, PWM_PERCENTAGE_TO_WIDTH(&FAN_DRV, val * 100));
  }else{
    pwmDisableChannel(&FAN_DRV, FAN_CH-1);
  }
}
