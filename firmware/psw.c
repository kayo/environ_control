#include "ch.h"
#include "hal.h"

#include "psw.h"

void pswInitObject(PSWDriver *psw){
  psw->config = NULL;
}

static PSWDriver *pool = NULL;

static PSWDriver *_pswFind(EXTDriver *extp, expchannel_t channel){
  PSWDriver *psw = pool;
  
  for(; psw != NULL; psw = psw->next){
    if(psw->config->input->extp == extp &&
       psw->config->input->pad == channel){
      return psw;
    }
  }
  
  return NULL;
}

static void _pswHandleIsr(EXTDriver *extp, expchannel_t channel){
  (void)extp;
  
  chSysLockFromIsr();
  
  {
    PSWDriver *psw = _pswFind(extp, channel);
    size_t i = 0, s = 100 / psw->config->outputs, j = s;
    
    for(; i < psw->config->outputs; i++, j += s){
      const PSWOutput *out = &psw->config->output[i];
      
      if((out->state == TRUE) == (psw->value >= j)){
        palSetPad(out->port, out->pad);
      }else{
        palClearPad(out->port, out->pad);
      }
    }
  }
  
  chSysUnlockFromIsr();
}

void pswStart(PSWDriver *psw, const PSWConfig *config){
  chDbgCheck(psw != NULL && config != NULL && config->outputs > 0,
             "pswStart");
  
  chSysLock();
  
  chDbgAssert(psw->config == NULL,
              "pswStart()", "invalid state");
  
  psw->config = config;

  { /* Configure outputs */
    size_t i = 0;

    for(; i < psw->config->outputs; i++){
      const PSWOutput *out = &psw->config->output[i];
      
      /* Deconfigure port */
      palSetPadMode(out->port, out->pad, out->mode);
    }
  }

  if(psw->config->input != NULL){
    const PSWInput *in = psw->config->input;
    
    { /* Add driver to queue */
      PSWDriver **pswp = &pool;
      
      psw->next = *pswp;
      *pswp = psw;
    }
    
    /* Configure port */
    palSetPadMode(in->port, in->pad, in->mode);
    
    { /* Configure ext channel */
      const EXTChannelConfig ch = {
        in->extm,
        _pswHandleIsr,
      };
      
      /* Turn on ext channel */
      extSetChannelModeI(in->extp, in->pad, &ch);
    }
  }
  
  chSysUnlock();
}

void pswStop(PSWDriver *psw){
  chDbgCheck(psw != NULL, "pswStop");
  
  chSysLock();
  
  chDbgAssert(psw->config != NULL,
              "pswStop()", "invalid state");

  if(psw->config->input != NULL){
    const PSWInput *in = psw->config->input;
    
    /* Turn off ext channel */
    extChannelDisable(in->extp, in->pad);
    
    /* Deconfigure port */
    palSetPadMode(in->port, in->pad, PAL_MODE_UNCONNECTED);
  
    { /* Remove driver from queue */
      PSWDriver **pswp = &pool;
      
      for(; *pswp != NULL; ){
        if(*pswp == psw){
          *pswp = psw->next;
          psw->next = NULL;
          break;
        }
      }
    }
  }

  { /* Deconfigure outputs */
    size_t i = 0;

    for(; i < psw->config->outputs; i++){
      const PSWOutput *out = &psw->config->output[i];
      
      /* Deconfigure port */
      palSetPadMode(out->port, out->pad, PAL_MODE_UNCONNECTED);
    }
  }
  
  psw->config = NULL;
  
  chSysUnlock();
}

void pswSet(PSWDriver *psw, size_t value){
  chDbgCheck(psw != NULL, "pswSet");
  
  chSysLock();
  
  chDbgAssert(psw->config != NULL,
              "pswSet()", "invalid state");
  
  psw->value = value;
  
  chSysUnlock();
}
