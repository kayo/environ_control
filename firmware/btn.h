/**
 * @brief The button driver configuration.
 */
typedef struct {
  /**
   * @brief The message, which will be sended when button event fired.
   */
  msg_t msg;
  /**
   * @brief The minimum time between button down and up to fire event.
   */
  systime_t time;
} BTNEvent;

#define BTN_EVENT_END {RDY_OK, 0}

/**
 * @brief The button driver configuration.
 */
typedef struct {
  /**
   * @brief The input port.
   */
  ioportid_t port;
  /**
   * @brief The input pad number.
   */
  uint16_t pad;
  /**
   * @brief The port pad mode.
   */
  iomode_t mode;
  /**
   * @brief The active state.
   */
  bool_t state;
  
  /**
   * @brief The ext driver to use.
   */
  EXTDriver *extp;
  /**
   * @brief The ext channel mode.
   */
  uint32_t extm;
  
  /**
   * @brief The mailbox to post button events.
   */
  Mailbox *mbox;
  
  /**
   * @brief The button events.
   *
   * Events must be sorted by delay time in asc order.
   * Last event must be empty (with time equalt to 0) to end detecting.
   */
  BTNEvent poll[];
} BTNConfig;

typedef struct BTNDriver BTNDriver;

/**
 * @brief The button driver object.
 */
struct BTNDriver {
  /**
   * @brief The driver configuration.
   */
  const BTNConfig *config;
  /**
   * @brief Next driver.
   */
  BTNDriver *next;
  /**
   * @brief The last triggering time.
   */
  systime_t last_time;
  /**
   * @brief The last button state.
   */
  bool_t last_state;
};

void btnInitObject(BTNDriver *btn);
void btnStart(BTNDriver *btn, const BTNConfig *config);
void btnStop(BTNDriver *btn);
