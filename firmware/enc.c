#include "ch.h"
#include "hal.h"

#include "enc.h"

void encInitObject(ENCDriver *enc){
  enc->config = NULL;
}

static ENCDriver *pool = NULL;

static ENCDriver *_encFind(EXTDriver *extp, expchannel_t channel){
  ENCDriver *enc = pool;
  
  for(; enc != NULL; enc = enc->next){
    if((enc->config->a.extp == extp &&
        enc->config->a.pad == channel) ||
       (enc->config->b.extp == extp &&
        enc->config->b.pad == channel)){
      return enc;
    }
  }
  
  return NULL;
}

static inline bool_t _encReadState(const ENCInput *config){
  return !config->state ^ palReadPad(config->port, config->pad);
}

static void _encHandleIsr(EXTDriver *extp, expchannel_t channel){
  (void)extp;
  
  chSysLockFromIsr();
  
  ENCDriver *enc = _encFind(extp, channel);
  
  /* Read current encoder inputs */
  uint8_t a = _encReadState(&enc->config->a) ? 1 : 0;
  uint8_t b = _encReadState(&enc->config->b) ? 1 : 0;
  
  /* Get current phase */
  uint8_t phase = (a << 1) | b;
  
  /* Get previous phase */
  uint8_t _phase = enc->phases & 0b11;
  
  if(phase != _phase){
    enc->phases <<= 2;
    enc->phases |= phase;

#define PHASES(_1, _2, _3, _4) (((_1)<<6)|((_2)<<4)|((_3)<<2)|((_4)<<0))
    
    switch(enc->phases){
    case PHASES(0, 2, 3, 1):
    case PHASES(3, 2, 3, 1):
      if(enc->config->mbox){
        chMBPostI(enc->config->mbox, enc->config->msg_b);
      }
      break;
    case PHASES(3, 2, 0, 1):
    case PHASES(0, 2, 0, 1):
      if(enc->config->mbox){
        chMBPostI(enc->config->mbox, enc->config->msg_a);
      }
      break;
    }
  }
  
  chSysUnlockFromIsr();
}

#define _encHandleAIsr _encHandleIsr
#define _encHandleBIsr _encHandleIsr

static void _startInput(const ENCInput *config, extcallback_t handler){
  /* Configure port */
  palSetPadMode(config->port, config->pad, config->mode);
  
  if(handler){ /* Configure ext channel */
    const EXTChannelConfig ch = {
      config->extm | EXT_CH_MODE_BOTH_EDGES, //(config->state ? EXT_CH_MODE_RISING_EDGE : EXT_CH_MODE_FALLING_EDGE),
      handler,
    };
    
    /* Turn on ext channel */
    extSetChannelModeI(config->extp, config->pad, &ch);
  }
}

void encStart(ENCDriver *enc, const ENCConfig *config){
  chDbgCheck(enc != NULL && config != NULL, "encStart");
  
  chSysLock();
  
  chDbgAssert(enc->config == NULL,
              "encStart()", "invalid state");
  
  enc->config = config;
  
  { /* Add encoder to queue */
    ENCDriver **encp = &pool;
    
    enc->next = *encp;
    *encp = enc;
  }
  
  enc->phases = 0;
  
  _startInput(&enc->config->a, _encHandleAIsr);
  _startInput(&enc->config->b, _encHandleBIsr);
  
  chSysUnlock();
}

static void _stopInput(const ENCInput *config, extcallback_t handler){
  /* Turn off ext channel */
  extChannelDisable(config->extp, config->pad);
  
  if(handler){
    /* Deconfigure port */
    palSetPadMode(config->port, config->pad, PAL_MODE_UNCONNECTED);
  }
}

void encStop(ENCDriver *enc){
  chDbgCheck(enc != NULL, "encStop");
  
  chSysLock();
  
  chDbgAssert(enc->config != NULL,
              "encStop()", "invalid state");
  
  _stopInput(&enc->config->a, _encHandleAIsr);
  _stopInput(&enc->config->b, _encHandleBIsr);

  { /* Remove encoder from queue */
    ENCDriver **encp = &pool;
    
    for(; *encp != NULL; ){
      if(*encp == enc){
        *encp = enc->next;
        enc->next = NULL;
        break;
      }
    }
  }
  
  enc->config = NULL;
  
  chSysUnlock();
}
