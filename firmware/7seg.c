#include "ch.h"
#include "hal.h"

#include "7seg.h"

void seg7InitObject(SEG7Driver *seg7){
  seg7->config = NULL;
  seg7->state = SEG7_STOP;
}

static inline void _seg7Switch(const SEG7Output *out, bool_t val){
  if(val){
    palSetPad(out->port, out->pad);
  }else{
    palClearPad(out->port, out->pad);
  }
}

static void _seg7UpdateMatrixes(SEG7Driver *seg7, SEG7Size pos, SEG7Size len){
  const SEG7Config *config = seg7->config;
  
  SEG7Size i, j;
  SEG7Value values[config->matrixes];
  
  /* collect values for current column */
  for(i = 0; i < config->signs; i++){
    const SEG7Sign *sign = &config->sign[i];
    if(sign->column == seg7->column){
      values[sign->matrix] = config->buffer[i];
    }
  }
  
  /* switch matrixes using collected values */
  for(i = pos; i < pos + len; i++){
    const SEG7Output *m = (const SEG7Output*)&config->matrix[i].a;
    for(j = 0; j < 8; j++){
      _seg7Switch(&m[j], !((values[i] >> j) & 0x1) ^ config->seg_active);
    }
  }
}

static void _seg7UpdateIsr(SEG7Driver *seg7);

static void _seg7DeadCycleIsr(SEG7Driver *seg7){
  const SEG7Config *config = seg7->config;
  
  chSysLockFromIsr();
  
  chVTSetI(&seg7->utimer, seg7->deadtime,
           (vtfunc_t)_seg7UpdateIsr, seg7);
  
  /* Turn off previous gate */
  _seg7Switch(&config->column[seg7->column], !config->gate_active);
  
  chSysUnlockFromIsr();
}

static void _seg7UpdateIsr(SEG7Driver *seg7){
  const SEG7Config *config = seg7->config;
  
  chSysLockFromIsr();
  
  /* get previous column and switch to next */
  SEG7Size column = seg7->column++;
  
  /* Cover column to columns count */
  if(seg7->column >= config->columns){
    seg7->column = 0;
  }
  
  chVTSetI(&seg7->utimer, seg7->period,
           (vtfunc_t)(/* It's the last column and has dead time */
                      seg7->column == config->columns - 1 && seg7->deadtime > 0 ?
                      /* Schedule dead cycle */
                      _seg7DeadCycleIsr :
                      /* Re-schedule updating task */
                      _seg7UpdateIsr), seg7);
  
  if(column < config->columns){
    /* Turn off previous gate */
    _seg7Switch(&config->column[column], !config->gate_active);
  }
  
  /* Switch segments */
  _seg7UpdateMatrixes(seg7, 0, config->matrixes);
  
  /* Turn on next gate */
  _seg7Switch(&config->column[seg7->column], config->gate_active);
  
  chSysUnlockFromIsr();
}

static void _seg7DutyCycle(SEG7Driver *seg7, uint8_t duty_cycle){
  const SEG7Config *config = seg7->config;

  systime_t period = config->period * duty_cycle / 255;
  
  /* Setup update period */
  seg7->period = period / config->columns;
  
  /* Setup dead time */
  seg7->deadtime = config->period - period;
}

void seg7DutyCycle(SEG7Driver *seg7, uint8_t duty_cycle){
  const SEG7Config *config = seg7->config;
  
  chDbgCheck(seg7 != NULL && config != NULL, "seg7DutyCycle");

  chSysLock();
  
  _seg7DutyCycle(seg7, duty_cycle);
  
  chSysUnlock();
}

void seg7Start(SEG7Driver *seg7, const SEG7Config *config){
  chDbgCheck(seg7 != NULL &&
             config != NULL &&
             config->matrix != NULL &&
             config->column != NULL &&
             config->buffer != NULL,
             "seg7Start");

  chSysLock();
  
  chDbgAssert(seg7->state == SEG7_STOP &&
              seg7->config == NULL,
              "seg7Start()", "invalid state");
  
  seg7->config = config;
  
  SEG7Size i, j;
  
  for(i = 0; i < config->matrixes; i++){
    const SEG7Output *m = (const SEG7Output *)&config->matrix[i].a;
    for(j = 0; j < 8; j++){
      /* Turn segment off */
      _seg7Switch(&m[j], !config->seg_active);
      /* Congifure matrix pads */
      palSetPadMode(m[j].port, m[j].pad, config->seg_mode);
    }
  }
  
  for(i = 0; i < config->columns; i++){
    const SEG7Output *c = &config->column[i];
    /* Turn off gate */
    _seg7Switch(c, !config->gate_active);
    /* Congifure gate pad */
    palSetPadMode(c->port, c->pad, config->gate_mode);
  }
  
  if(config->columns > 0){
    /* Switch to last column */
    seg7->column = config->columns;
    
    _seg7DutyCycle(seg7, config->duty_cycle);
  }
  
  seg7->state = SEG7_READY;
  
  chSysUnlock();
  
  if(config->columns > 0 && config->period > 0){
    /* Schedule updating task */
    chVTSet(&seg7->utimer, seg7->period,
            (vtfunc_t)_seg7UpdateIsr, seg7);
  }
}

void seg7Stop(SEG7Driver *seg7){
  chDbgCheck(seg7 != NULL, "seg7Stop");
  
  const SEG7Config *config = seg7->config;
  
  /* Cancel updating task */
  chVTReset(&seg7->utimer);
  
  chSysLock();
  
  chDbgAssert(seg7->state != SEG7_STOP &&
              config != NULL,
              "seg7Stop()", "invalid state");
  
  seg7->config = NULL;
  
  SEG7Size i, j;
  
  for(i = 0; i < config->matrixes; i++){
    const SEG7Output *m = (const SEG7Output *)&config->matrix[i].a;
    for(j = 0; j < 8; j++){
      /* De-congifure matrix pads */
      palSetPadMode(m[j].port, m[j].pad, PAL_MODE_UNCONNECTED);
    }
  }
  
  for(i = 0; i < config->columns; i++){
    const SEG7Output *c = &config->column[i];
    /* De-congifure gate pad */
    palSetPadMode(c->port, c->pad, PAL_MODE_UNCONNECTED);
  }
  
  seg7->state = SEG7_STOP;
  
  chSysUnlock();
}

#if SEG7_USE_SET
#include "string.h"

void seg7Set(SEG7Driver *seg7, SEG7Size pos, SEG7Size len, const SEG7Value *sign){
  chDbgCheck(seg7 != NULL,
             "seg7Set");

  const SEG7Config *config = seg7->config;
  
  chSysLock();
  
  chDbgAssert(seg7->state == SEG7_READY &&
              config != NULL &&
              pos + len <= config->signs,
              "seg7Set()", "invalid state");
  
  memcpy(config->buffer + pos, sign, len);
  
  if(config->columns == 0){
    /* Switch segments when static indication is used */
    _seg7UpdateMatrixes(seg7, pos, len);
  }
  
  chSysUnlock();
}

#endif//SEG7_USE_SET

#if SEG7_USE_PUT || SEG7_USE_PRINTF

static const SEG7Value _seg7Signs[] = {
  [' '] = 0b00000000,
  ['!'] = 0b00110000,
  ['"'] = 0b00100010,
  ['\''] = 0b00000010,
  ['('] = 0b00110001,
  [')'] = 0b00001110,
  ['*'] = 0b01100011,
  [','] = 0b00000100,
  ['-'] = 0b01000000,
  ['.'] = 0b10000000,
  ['/'] = 0b00010000,
  
  ['0'] = 0b00111111,
  ['1'] = 0b00000110,
  ['2'] = 0b01011011,
  ['3'] = 0b01001111,
  ['4'] = 0b01100110,
  ['5'] = 0b01101101,
  ['6'] = 0b01111101,
  ['7'] = 0b00000111,
  ['8'] = 0b01111111,
  ['9'] = 0b01101111,

  ['<'] = 0b01010000,
  ['='] = 0b01001000,
  ['>'] = 0b01000100,
  ['?'] = 0b01010011,
  
  ['A'] = 0b01110111,
  ['B'] = 0b01111100,
  ['C'] = 0b00111001,
  ['D'] = 0b01011110,
  ['E'] = 0b01111001,
  ['F'] = 0b01110001,
  ['G'] = 0b01101111,
  ['H'] = 0b01110110,
  ['I'] = 0b00110000,
  ['J'] = 0b00110000,
  ['K'] = 0b01110000,
  ['L'] = 0b00111000,
  ['M'] = 0b00110011, /* |^' */
  ['N'] = 0b00110111,
  ['O'] = 0b00111111,
  ['P'] = 0b01110011,
  ['Q'] = 0b01100111,
  ['S'] = 0b01101101,
  ['T'] = 0b01111000,
  ['U'] = 0b00111110,
  ['V'] = 0b00011100,
  ['Y'] = 0b01101110,
  
  ['['] = 0b00111001,
  [']'] = 0b00001111,
  ['^'] = 0b00100011,
  ['_'] = 0b00001000,
  ['`'] = 0b00100000,
  
  ['a'] = 0b01110111,
  ['b'] = 0b01111100,
  ['c'] = 0b01011000,
  ['d'] = 0b01011110,
  ['e'] = 0b01111011,
  ['f'] = 0b01110001,
  ['g'] = 0b01101111,
  ['h'] = 0b01110100,
  ['i'] = 0b00110000,
  ['j'] = 0b00110000,
  ['k'] = 0b01110000,
  ['l'] = 0b00111000,
  ['m'] = 0b00100111, /* '^| */
  ['n'] = 0b01010100,
  ['o'] = 0b01011100,
  ['p'] = 0b01110011,
  ['q'] = 0b01100111,
  ['s'] = 0b01101101,
  ['t'] = 0b01111000,
  ['u'] = 0b00111110,
  ['v'] = 0b00011100,
  ['y'] = 0b01101110,
  
  ['{'] = 0b01000110,
  ['|'] = 0b00110000,
  ['}'] = 0b01110000,
  ['~'] = 0b00000001,
};

static void _seg7Out(SEG7Driver *seg7, SEG7Size pos, const char *str){
  const SEG7Config *config = seg7->config;
  bool_t or_mode = FALSE;
  const char *c = str;
  SEG7Size i = pos;
  
  for(; *c != '\0' && i < config->signs; c++){
    if(or_mode == TRUE){
      if(*c == ';'){
        or_mode = FALSE;
        i++;
      }else{
        config->buffer[i] |= _seg7Signs[(size_t)*c];
      }
    }else if(*c == '&'){
      config->buffer[i] = 0x0;
      or_mode = TRUE;
    }else if(*c == '\b'){ /* cursor backward */
      i--;
    }else if(*c == '\f'){ /* cursor forward */
      i++;
    }else if(*c == '.' && c != str){
      config->buffer[i-1] |= _seg7Signs[(size_t)'.'];
    }else{
      config->buffer[i++] = _seg7Signs[(size_t)*c];
    }
  }
  
  if(config->columns == 0){
    /* Switch segments when static indication is used */
    _seg7UpdateMatrixes(seg7, pos, i - pos);
  }
}

#endif//SEG7_USE_PUT || SEG7_USE_PRINTF

#if SEG7_USE_PUT

void seg7Put(SEG7Driver *seg7, SEG7Size pos, const char *line){
  chDbgCheck(seg7 != NULL,
             "seg7Put");
  
  chDbgAssert(seg7->state == SEG7_READY &&
              seg7->config != NULL,
              "seg7Put()", "invalid state");
  
  chSysLock();
  
  _seg7Out(seg7, pos, line);
  
  chSysUnlock();
}

#endif//SEG7_USE_PUT

#if SEG7_USE_PRINTF
#include "memstreams.h"
#include "chprintf.h"

void seg7Printf(SEG7Driver *seg7, SEG7Size pos, const char *fmt, ...){
  va_list ap;
  MemoryStream ms;

  chDbgCheck(seg7 != NULL,
             "seg7Printf");
  
  chDbgAssert(seg7->state == SEG7_READY &&
              seg7->config != NULL,
              "seg7Printf()", "invalid state");
  
  SEG7Size len = (seg7->config->signs - pos) << 2;
  char str[len + 1];
  
  /* Memory stream object to be used as a string writer, reserving one
     byte for the final zero.*/
  msObjectInit(&ms, (uint8_t *)str, len, 0);
  
  /* Performing the print operation using the common code.*/
  va_start(ap, fmt);
  chvprintf((BaseSequentialStream*)&ms, fmt, ap);
  va_end(ap);
  
  /* Final zero and size return.*/
  str[ms.eos] = 0;
  
  chSysLock();
  
  _seg7Out(seg7, pos, str);
  
  chSysUnlock();
}

#endif//SEG7_USE_PRINTF
