void localExtStart(void);
void localExtStop(void);

void localAdcStart(void);
void localAdcStop(void);

void localPwmStart(void);
void localPwmStop(void);

void localRtcInit(void);
void localRtcStart(void);
void localRtcStop(void);

void localTrmInit(void);
void localTrmStart(void);
void localTrmStop(void);

#ifndef CTL_USE_THREAD
#define CTL_USE_THREAD FALSE
#endif

void localCtlInit(void);
void localCtlStart(void);
void localCtlStop(void);
#if !CTL_USE_THREAD
void localCtlMain(void);
#endif

void localUiInit(void);
void localUiStart(void);
void localUiStop(void);

void fanSet(float);
