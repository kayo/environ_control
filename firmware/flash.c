#include "ch.h"
#include "hal.h"

#include "string.h"
#include "flash.h"

#if FLASH_USE_MUTEX || FLASH_USE_SEMAPHORE

#if FLASH_USE_MUTEX && FLASH_USE_SEMAPHORE
#error "Please disable FLASH_USE_MUTEX or FLASH_USE_SEMAPHORE"
#endif

#if FLASH_USE_MUTEX
static MUTEX_DECL(mutex);
#define _flashLock() chMtxLock(&mutex)
#define _flashUnlock() chMtxUnlock()
#endif

#if FLASH_USE_SEMAPHORE
static BSEMAPHORE_DECL(bsem, FALSE);
#define _flashLock() chBSemWait(&bsem)
#define _flashUnlock() chBSemSignal(&bsem)
#endif

#else/*FLASH_USE_MUTEX || FLASH_USE_SEMAPHORE*/
#warning "Flashing in multithreading maner may be unsafe! Please enable FLASH_USE_MUTEX or FLASH_USE_SEMAPHORE"
#define _flashLock()
#define _flashUnlock()
#endif/*FLASH_USE_MUTEX || FLASH_USE_SEMAPHORE*/

#ifndef FLASH_KEY1
#define FLASH_KEY1 ((uint32_t)0x45670123)
#endif

#ifndef FLASH_KEY2
#define FLASH_KEY2 ((uint32_t)0xcdef89ab)
#endif

void flashLock(){
  if(!(FLASH->CR & FLASH_CR_LOCK)){
    FLASH->CR |= FLASH_CR_LOCK;
  }
}

void flashUnlock(){
  if(FLASH->CR & FLASH_CR_LOCK){
    FLASH->KEYR = FLASH_KEY1;
    FLASH->KEYR = FLASH_KEY2;
  }
}

#if FLASH_USE_INTERRUPT

#ifndef STM32_FLASH_IRQ_PRIORITY
#error "Please define STM32_FLASH_IRQ_PRIORITY in mcuconf.h"
#endif

static BSEMAPHORE_DECL(bsemWait, TRUE);
static uint32_t SR = 0;

static FlashStatus _flashStatus(void){
  FlashStatus status = FlashSuccess;

  if(SR & FLASH_SR_PGERR){
    status |= FlashNotEmpty;
  }

  if(SR & FLASH_SR_WRPRTERR){
    status |= FlashProtected;
  }
  
  SR = 0;
  
  return status;
}

/**
 * @brief   Flash interrupt handler.
 *
 * @isr
 */
CH_IRQ_HANDLER(FLASH_IRQHandler){
  CH_IRQ_PROLOGUE();

  chSysLockFromIsr();
  
  /* Save status of last operation */
  SR = FLASH->SR;
  
  /* Reset end of operation flag and error flags */
  FLASH->SR |= FLASH_SR_EOP | FLASH_SR_WRPRTERR | FLASH_SR_PGERR;
  
  /* Notify thread when operation complete */
  chBSemSignalI(&bsemWait);
  
  chSysUnlockFromIsr();
  
  CH_IRQ_EPILOGUE();
}

static void _flashInit(void){
  /* In case when flash controller is BuSY we need to wait until it will be accessible */
  for(; (FLASH->SR & FLASH_SR_BSY); );
  
  /* Enable flash interrupts */
  FLASH->CR |= FLASH_CR_EOPIE | FLASH_CR_ERRIE; /* End of OPeration and ERRor */

  /* Enable flash interrupt vector */
  nvicEnableVector(FLASH_IRQn, CORTEX_PRIORITY_MASK(STM32_FLASH_IRQ_PRIORITY));
}

static void _flashWait(void){
  /* Wait until semaphore will be signaled by the interrupt */
  chBSemWait(&bsemWait);
}

static void _flashDone(void){
  /* Disable flash interrupt vector */
  nvicDisableVector(FLASH_IRQn);
  
  /* Disable flash interrupts */
  FLASH->CR &= ~(FLASH_CR_EOPIE | FLASH_CR_ERRIE); /* End of OPeration and ERRor */
}

#else

static FlashStatus _flashStatus(void){
  FlashStatus status = FlashSuccess;

  if(FLASH->SR & FLASH_SR_PGERR){
    status |= FlashNotEmpty;
  }

  if(FLASH->SR & FLASH_SR_WRPRTERR){
    status |= FlashProtected;
  }
  
  /* Reset last operation end bit and errors */
  FLASH->SR |= FLASH_SR_EOP | FLASH_SR_WRPRTERR | FLASH_SR_PGERR;
  
  return status;
}

static void _flashInit(void){
  /* In case when flash controller is BuSY we need to wait until it will be accessible */
  for(; (FLASH->SR & FLASH_SR_BSY); );
  
#if FLASH_USE_EOP
  /* Using End of OPeration bit */
  if(FLASH->SR & FLASH_SR_EOP){
    FLASH->SR |= FLASH_SR_EOP;
  }
#endif
}

static void _flashWait(void){
#if FLASH_USE_EOP
  /* Using End of OPeration bit */
  for(; !(FLASH->SR & FLASH_SR_EOP); );
  FLASH->SR |= FLASH_SR_EOP;
#else
  /* Using BuSY bit */
  for(; (FLASH->SR & FLASH_SR_BSY); );
#endif
}

static void _flashDone(void){
  /* Nothing to do here */
}

#endif

FlashStatus flashErase(uint32_t address){
  FlashStatus status;
  
  _flashLock();
  
  _flashInit();
  
  FLASH->CR |= FLASH_CR_PER; /* Page Erase on */
  FLASH->AR = address;
  FLASH->CR |= FLASH_CR_STRT; /* Start */
  
  _flashWait();

  status = _flashStatus();
  
  FLASH->CR &= ~FLASH_CR_PER; /* Page Erase off */
  
  _flashDone();

  _flashUnlock();

  return status;
}

FlashStatus flashWrite(uint32_t address, const uint8_t* data, size_t size){
  FlashStatus status = FlashSuccess;
  
  _flashLock();
  
  _flashInit();
  
  FLASH->CR |= FLASH_CR_PG; /* Programming on */
  
  for(; size > 1 && status == FlashSuccess;
      address += 2, size -= 2, data += 2){ /* write byte pairs */
    *((__IO uint16_t*)address) = ((uint16_t)data[0]) | (((uint16_t)data[1]) << 8);
    
    _flashWait();
    
    status = _flashStatus();
  }
  
  if(size > 0 && status == FlashSuccess){ /* need write only one last byte */
    *((__IO uint16_t*)address) = (uint16_t)data[0];
    
    _flashWait();

    status = _flashStatus();
  }
  
  FLASH->CR &= ~(FLASH_CR_PG); /* Programming off */
  
  _flashDone();
  
  _flashUnlock();
  
  return status;
}

FlashStatus flashCheck(uint32_t address, const uint8_t* data, size_t size){
  FlashStatus status = FlashSuccess;
  
  _flashLock();
  
  for(; size > 0; address ++, size --, data ++){ /* check bytes */
    if((*(__IO uint8_t*)address) != data[0]){
      status = FlashInvalid;
      break;
    }
  }
  
  _flashUnlock();
  
  return status;
}
