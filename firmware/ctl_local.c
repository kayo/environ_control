#include "ch.h"
#include "hal.h"

#include "local.h"
#include "rtc_event.h"
#include "psw.h"
#include "par.h"

/* Used parameters */

extern PARObject rtcYear;
extern PARObject rtcMon;
extern PARObject rtcDate;
extern PARObject rtcHour;
extern PARObject rtcMin;
extern PARObject rtcSec;
extern PARObject rtcDay;

extern PARObject trmPar[];

/* Mode parameter */

int mode;
static const int modeDef = 1, modeMin = 1, modeMax = 9, modeStp = 1;

static const PARConfig modeCfg = {
  0x00010001, "mode", "PEKH", NULL,
  "Current mode of operation.\n\n"
  "1 - Automatic normal mode.\n"
  "2 - Automatic economy mode.\n"
  "3 - Manual mode.\n",
  &PARInt, PARReadable | PARWritable | PARPersist,
  &mode, sizeof(mode), NULL, NULL,
  &modeDef, &modeMin, &modeMax, &modeStp
};

PARObject modePar;

/* Tarif parameter */

static msg_t tarifGet(const PARConfig *config, PARCell *val){
  (void)config;
  
  int hour;
  msg_t res = parGet(&rtcHour, &hour);
  
  if(res == RDY_OK){
    *((int*)val) = hour > 6 && hour < 23 ? 2 : 1;
  }
  
  return res;
}

static const PARConfig tarifCfg = {
  0x00010001, "tarif", "TAPU", NULL,
  "Current energy tarif.",
  &PARInt, PARReadable,
  NULL, 0, tarifGet, NULL,
  NULL, NULL, NULL, NULL
};

PARObject tarifPar;

static const PSWInput zero_det = {
  PORT(ZERO_DT),
  PAD(ZERO_DT),
  PAL_MODE_INPUT_PULLUP,
  FALSE,
  &ZERO_DRV,
  EXT(ZERO_DT) | EXT_CH_MODE_RISING_EDGE,
};

static const PSWConfig psw_cfg = {
  &zero_det,
  3,
  {
    {
      PORT(LOAD_1),
      PAD(LOAD_1),
      PAL_MODE_OUTPUT_OPENDRAIN,
      FALSE,
    },
    {
      PORT(LOAD_2),
      PAD(LOAD_2),
      PAL_MODE_OUTPUT_OPENDRAIN,
      FALSE,
    },
    {
      PORT(LOAD_3),
      PAD(LOAD_3),
      PAL_MODE_OUTPUT_OPENDRAIN,
      FALSE,
    },
  },
};

PSWDriver PSWD1;

int power;

static const int powerDef = 0, powerMin = 0, powerMax = 100, powerStp = 1;

static msg_t powerSet(const PARConfig *config, const PARCell *val){
  (void)config;
  
  pswSet(&PSWD1, *((int*)val));
  
  return RDY_OK;
}

static const PARConfig powerCfg = {
  0x00010001, "power", "HA&|^;P", NULL,
  "Current heater power.",
  &PARInt, PARReadable | PARWritable,
  &power, sizeof(power), NULL, powerSet,
  &powerDef, &powerMin, &powerMax, &powerStp
};

PARObject powerPar;

/* Temperature control parameters */

#define celsius "%2d*C"

#define airNum 2
#define airTMinI 0
#define airTOptI 1

int air[airNum];
static const int airTDef[] = {7, 19}, airTMin = -4, airTMax = 30, airTStp = 1;

static const PARConfig airCfg[] = {
  {
    0x00010001, "air.t.min", "tA _", celsius,
    "Minimal air temperature",
    &PARInt, PARReadable | PARWritable | PARPersist,
    &air[0], sizeof(air[0]), NULL, NULL,
    &airTDef[0], &airTMin, &airTMax, &airTStp
  },
  {
    0x00010001, "air.t.opt", "tA -", celsius,
    "Optimal air temperature",
    &PARInt, PARReadable | PARWritable | PARPersist,
    &air[1], sizeof(air[1]), NULL, NULL,
    &airTDef[1], &airTMin, &airTMax, &airTStp
  }
};

PARObject airPar[airNum];

#define oilNum 2
#define oilTMinI 0
#define oilTMaxI 1

int oil[oilNum];
static const int oilTDef[] = {3, 20}, oilTMin = 4, oilTMax = 70, oilTStp = 1;

static const PARConfig oilCfg[] = {
  {
    0x00010001, "oil.t.min", "tO _", celsius,
    "Minimal oil temperature",
    &PARInt, PARReadable | PARWritable | PARPersist,
    &oil[0], sizeof(oil[0]), NULL, NULL,
    &oilTDef[0], &oilTMin, &oilTMax, &oilTStp
  },
  {
    0x00010001, "oil.t.max", "tO ~", celsius,
    "Optimal oil temperature",
    &PARInt, PARReadable | PARWritable | PARPersist,
    &oil[1], sizeof(oil[1]), NULL, NULL,
    &oilTDef[1], &oilTMin, &oilTMax, &oilTStp
  }
};

PARObject oilPar[oilNum];

/*
 * Control thread
 */

static BinarySemaphore bsem;

/*
 * Clock handler
 */

static void sendClock(RTCDriver *rtcp, rtcevent_t event){
  (void)rtcp;
  (void)event;
  
  chSysLockFromIsr();
  
  chBSemSignalI(&bsem);
  
  chSysUnlockFromIsr();
}

static const RTCHandlerConfig rtch_clock_cfg = {
  &RTCD1,
  sendClock,
  {
    RTC_EVENT_SECOND,
    RTC_EVENT_END,
  }
};

static RTCHandler rtch_clock;

typedef struct {
  bool_t has;
  int min, max;
} IntRange;

static void intRange(IntRange *range, int value){
  if(!range->has){
    range->min = range->max = value;
    range->has = TRUE;
  }else{
    if(value < range->min){
      range->min = value;
    }
    if(value > range->max){
      range->max = value;
    }
  }
}

static void _fanCtl(void){
  static const int n_t = 42;
  static const float p_k = 10.0;
  int t;
  
  /* internal thermistor */
  if(RDY_OK == parGet(&trmPar[4], &t)){
    float p_t = p_k * (t - n_t);
    
    if(p_t > 10){
      fanSet(p_t);
    }else{
      fanSet(0);
    }
  }
}

static void _main(void){
  rtcHandlerStart(&rtch_clock, &rtch_clock_cfg);
  
  for(; chBSemWait(&bsem) == RDY_OK; ){
    _fanCtl();
    
    if(mode != 9){
      int i, tarif, power, value;
      IntRange air_t, oil_t;
      
      parGet(&tarifPar, &tarif);
      
      for(i = 0; i < 2; i++){
        if(RDY_OK == parGet(&trmPar[i], &value)){
          intRange(&air_t, value);
        }
      }
      
      for(i = 2; i < 4; i++){
        if(RDY_OK == parGet(&trmPar[i], &value)){
          intRange(&oil_t, value);
        }
      }
      
      power = oil_t.has && oil_t.min < oil[oilTMinI] ? 100 :
        oil_t.has && oil_t.max > oil[oilTMaxI] ? 0 :
        mode == 2 && air_t.has && air_t.min < air[airTOptI] ? 100 :
        mode == 1 && air_t.has && air_t.max < air[airTMinI] ? 100 :
        0;
      
      parSet(&powerPar, &power);
    }
  }
  
  rtcHandlerStop(&rtch_clock);
}

#if CTL_USE_THREAD
static WORKING_AREA(thread_wa, 512);
static msg_t thread_fn(void *arg){
  (void)arg;

  chRegSetThreadName("ctl");
  
  _main();
  
  return 0;
}
#else
void localCtlMain(void){
  _main();
}
#endif

/* Interface */

void localCtlInit(void){
  size_t i;

  pswInitObject(&PSWD1);
  
  parInitObject(&modePar);
  parInitObject(&tarifPar);
  parInitObject(&powerPar);

  for(i = 0; i < airNum; i++){
    parInitObject(&airPar[i]);
  }

  for(i = 0; i < oilNum; i++){
    parInitObject(&oilPar[i]);
  }
}

void localCtlStart(void){
  size_t i;

  pswStart(&PSWD1, &psw_cfg);
  
  parStart(&modePar, &modeCfg);
  parStart(&tarifPar, &tarifCfg);
  parStart(&powerPar, &powerCfg);

  for(i = 0; i < airNum; i++){
    parStart(&airPar[i], &airCfg[i]);
  }

  for(i = 0; i < oilNum; i++){
    parStart(&oilPar[i], &oilCfg[i]);
  }
  
  chBSemInit(&bsem, FALSE);

#if CTL_USE_THREAD
  chThdCreateStatic(thread_wa, sizeof(thread_wa), NORMALPRIO, thread_fn, NULL);
#endif
}

void localCtlStop(void){
  size_t i;
  
  chBSemReset(&bsem, FALSE);

  pswStop(&PSWD1);
  
  parStop(&modePar);
  parStop(&tarifPar);
  parStop(&powerPar);

  for(i = 0; i < airNum; i++){
    parStop(&airPar[i]);
  }

  for(i = 0; i < oilNum; i++){
    parStop(&oilPar[i]);
  }
}
