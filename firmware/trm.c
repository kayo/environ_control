#include "ch.h"
#include "hal.h"

#include "trm.h"

#include <math.h>

#ifndef ADC_BITS
#define ADC_BITS 12
#endif

static void _trmConvert(TRMDriver *trm){
  const TRMConfig *config = trm->config;

  if(!(trm->flags & TRMNeedConvert)){
    return;
  }

#define r trm->value
  
  r = (double)trm->sample / ((1 << ADC_BITS) - 1);
#define R1 config->options->r1
#define R2 config->options->r2
  r = R2 > 0 ? r * R1 * R2 / ((1 - r) * R2 - r * R1) : r * R1 / (1 - r);
#undef R1
#undef R2
  
  switch(config->params->model){
  case TRMModelBeta: {
    TRMParamsBeta *params = (TRMParamsBeta*)config->params;
    r = 1.0 / (1.0 / params->t0 + 1.0 / params->beta * log(r / params->r0));
  } break;
  case TRMModelSHH: {
    TRMParamsSHH *params = (TRMParamsSHH*)config->params;
    r = log(r);
    r = 1.0 / (params->a + params->b * r + params->c * pow(r, 3));
  } break;
  }
  
  switch(config->options->units){
  case TRMKelvins:
    break;
  case TRMCelsius:
    r -= 273.15;
    break;
  case TRMFarenheits:
    r -= 273.15;
    r *= 9.0 / 5.0;
    r += 32.0;
  }
  
  trm->flags &= ~TRMNeedConvert;
  
#undef r
}

void trmInitObject(TRMDriver *trm){
  trm->config = NULL;
}

void trmStart(TRMDriver *trm, const TRMConfig *config){
  chDbgCheck(trm != NULL && config != NULL && config->params != NULL && config->options != NULL, "trmStart");
  
  chSysLock();
  
  chDbgAssert(trm->config == NULL,
              "trmStart()", "invalid state");
  
  trm->config = config;
  trm->flags = TRMNoFlags;
  
  palSetPadMode(config->port, config->pad, PAL_MODE_INPUT_ANALOG);

#if TRM_USE_SEMAPHORE
  chBSemInit(&trm->bsem, TRUE);
#endif
  
  chSysUnlock();
}

void trmStop(TRMDriver *trm){
  chDbgCheck(trm != NULL, "trmStop");
  
  chSysLock();
  
  chDbgAssert(trm->config != NULL,
              "trmStop()", "invalid state");

#if TRM_USE_SEMAPHORE
  chBSemReset(&trm->bsem, TRUE);
#endif
  
  palSetPadMode(trm->config->port, trm->config->pad, PAL_MODE_UNCONNECTED);
  
  trm->config = NULL;
  
  chSysUnlock();
}

void trmUpdateI(TRMDriver *trm, adcsample_t val){
  chDbgCheck(trm != NULL && trm->config != NULL, "trmUpdateI");
  
  trm->sample = val;
  trm->flags |= TRMAvailable | TRMNeedConvert;
  
#if TRM_USE_SEMAPHORE
  chBSemSignalI(&trm->bsem);
#endif

#if TRM_USE_MAILBOX
  if(trm->config->mbox != NULL){
    chMBPostI(trm->config->mbox, trm->config->msg);
  }
#endif

#if TRM_USE_CALLBACK
  if(trm->config->cb){
    trm->config->cb(trm);
  }
#endif
}

#if TRM_USE_SEMAPHORE
msg_t trmGetNewTimeout(TRMDriver *trm, double *value, systime_t time){
  chDbgCheck(trm != NULL && trm->config != NULL, "trmGetTimeout");
  
  msg_t status = chBSemWaitTimeout(&trm->bsem, time);
  
  if(status == RDY_OK){
    _trmConvert(trm);
    
    *value = trm->value;
  }
  
  return status;
}
#endif

msg_t trmGetLast(TRMDriver *trm, double *value){
  chDbgCheck(trm != NULL && trm->config != NULL, "trmGet");
  
  if(trm->flags & TRMAvailable){
    _trmConvert(trm);
    
    *value = trm->value;
    
    return RDY_OK;
  }
  
  return RDY_RESET;
}
