module board(
    board/*=[width, length, thickness]*/,
    bound/*=[bottom, top]*/,
    holes/*=[[x, y, diameter]]*/,
    boxes/*=[[width, length, height, x, y, bottom]]*/,
){
    color("darkgreen")
    difference(){
        translate([-board[0]/2, -board[1]/2, 0])
        cube([board[0], board[1], board[2]]);

        for(hole = holes)
        translate([hole[0], hole[1], -$fix])
        cylinder(r=hole[2]/2, board[2]+$fix*2);
    }

    color("brown")
    for(box=boxes)
    translate([box[3]-box[0]/2, box[4]-box[1]/2, box[5]?-box[2]:board[2]])
    cube([box[0], box[1], box[2]]);
    
    /* bottom bound */
    %translate([-board[0]/2, -board[1]/2, -bound[0]])
    cube([board[0], board[1], bound[0]-$fix]);

    /* top bound */
    %translate([-board[0]/2, -board[1]/2, board[2]+$fix])
    cube([board[0], board[1], bound[1]-$fix]);
}

module power_board(
    board=[ 84, 57, 1.5 ],
    bound=[ 5, 20 ],
    holes=[
        [ -37, -12.5, 3 ],
        [ -37,  12.5, 3 ],
        [  37, -12.5, 3 ],
        [  37,  12.5, 3 ],
        [ -37, -22.5, 3 ],
        [ -37,  22.5, 3 ],
        [  37, -22.5, 3 ],
        [  37,  22.5, 3 ],
    ],
    boxes=[
        [ 10, 25, 5,   5.5, 22.5, 1 ],
        [ 10, 25, 5, -11.0, 22.5, 1 ],
        [ 10, 25, 5, -27.5, 22.5, 1 ],
    ],
){
    board(board, bound, holes, boxes);
}

module logic_board(
    board=[ 84, 60.5, 1.5 ],
    bound=[ 5, 20 ],
    holes=[
        [ -37,  -4.5, 3 ],
        [  37,  -4.5, 3 ],
        [ -37, -24.5, 3 ],
        [  37, -24.5, 3 ],
        [ -37,  15.5, 3 ],
        [  37,  15.5, 3 ],
    ],
    boxes=[
        [ 12.3, 13, 14,     9, 24.0 ],
        [ 12.3, 13, 14,  26.7, 24.0 ],
        [ 12.3, 13, 14,    -9, 24.0 ],
        [ 12.3, 13, 14, -26.7, 24.0 ],
        [ 50, 19.5, 12,  -8.5, 7    ],
    ]
){
    board(board, bound, holes, boxes);
}

module heatsink(
    base=[
        65+1,  /* width */
        105+1, /* length */
        22,    /* height */
    ],
    pocket=[
        13,  /* count */
        1.5, /* thickness */
        18,  /* height */
    ],
    outer=0,
){
    step = (base[1]-pocket[1])/pocket[0];
    width = step - pocket[1];
    
    color("lightgray")
    difference(){
        if(outer == 0){
            translate([-base[0]/2, -base[1]/2, -base[2]])
            cube([base[0], base[1], base[2]]);
        }
        
        for(pos=[-base[1]/2+pocket[1]:step:base[1]/2-$fix])
        translate([-base[0]/2-$fix-outer, pos, -base[2]-$fix])
        cube([base[0]+$fix*2+outer*2, width, pocket[2]+$fix]);
    }
}

module cooler(
    base=[
        60, /* width */
        15, /* height */
        5,  /* bevel radius */
    ]
){
    color("darkgray")
    translate([-base[0]/2+base[2], -base[0]/2+base[2], 0])
    minkowski(){
        cube([base[0]-base[2]*2, base[0]-base[2]*2, base[1]/2]);
        cylinder(r=base[2], h=base[1]/2);
    }
}

module housing(
    base=[
        5, /* width border */
        3, /* length border */
        5, /* bevel radius */
    ],
    board=[
        84,  /* width */
        57 + 5 + 60.6,  /* length */
        1.5 + 20, /* height */
    ],
    under=[
        84-7*2,  /* width */
        57 + 60.6,  /* length */
        5, /* depth */
    ],
    heatsink=[
        65+1,  /* width */
        105+1, /* length */
        22,    /* height */
    ],
    heatsink_pocket=[
        13,  /* count */
        3, /* thickness */
        18,  /* height */
    ],
    cooler=[
        60, /* width */
        15, /* height */
        5,  /* bevel radius */
    ],
    hole_groups=[
        [
            [0, -34, 0.2],
            [
                /* power board holes */
                [ -37, -12.5, 3 ],
                [ -37,  12.5, 3 ],
                [  37, -12.5, 3 ],
                [  37,  12.5, 3 ],
                [ -37, -22.5, 3 ],
                [ -37,  22.5, 3 ],
                [  37, -22.5, 3 ],
                [  37,  22.5, 3 ],
            ],
        ],
        [
            [0, 32, 0.2],
            [
                /* logic board holes */
                [ -37,  -4.5, 3 ],
                [  37,  -4.5, 3 ],
                [ -37, -24.5, 3 ],
                [  37, -24.5, 3 ],
                [ -37,  15.5, 3 ],
                [  37,  15.5, 3 ],
            ],
        ],
    ],
    header_groups=[
        [
            [0, -34, 4],
            [
                [ 5, 13+4, 10,     9, -24.0 ],
                [ 5, 13+4, 10,  26.7, -24.0 ],
                [ 5, 13+4, 10,    -9, -24.0 ],
                [ 5, 13+4, 10, -26.7, -24.0 ],
            ]
        ],
        [
            [0, 32, 3],
            [
                [ 10, 13+4, 12,     9, 24.0 ],
                [ 10, 13+4, 12,  26.7, 24.0 ],
                [ 10, 13+4, 12,    -9, 24.0 ],
                [ 10, 13+4, 12, -26.7, 24.0 ],
            ],
        ],
    ],
){
    bound = [
        max(board[0], heatsink[0], cooler[0]) + base[0]*2,
        max(board[1], heatsink[1], cooler[0]) + base[1]*2,
        board[2] + under[2] + heatsink[2] + cooler[1],
    ];
    offset = heatsink[2] + cooler[1] + under[2]; /* z offset */
    
    echo("bound:", bound);
    
    difference(){
        union(){
            translate([-bound[0]/2+base[2], -bound[1]/2+base[2], -offset])
            minkowski(){
                cube([bound[0]-base[2]*2, bound[1]-base[2]*2, bound[2]/2]);
                cylinder(r=base[2], h=bound[2]/2);
            }

            *for(mx=[0, 1])
            mirror([mx, 0, 0])
            for(my=[0, 1])
            mirror([0, my, 0])
            translate([bound[0]/2, bound[1]/4, ])
            cylinder(r=10, h=7);
        }
        
        /* board */
        translate([-board[0]/2-$clr, -board[1]/2-$clr/2, 0])
        cube([board[0]+$clr*2, board[1]+$clr*2, board[2]+$clr+$fix]);

        /* board holes */
        for(hole_group=hole_groups)
        for(hole=hole_group[1])
        translate([hole_group[0][0]+hole[0], hole_group[0][1]+hole[1], -under[2]-(heatsink[2]-heatsink_pocket[2])+$fix])
        cylinder(r=hole[2]/2-hole_group[0][2], under[2]+(heatsink[2]-heatsink_pocket[2])+$fix);

        /* header pockets */
        for(header_group=header_groups)
        for(header=header_group[1])
        translate([header_group[0][0]+header[3]-header[0]/2, header_group[0][1]+header[4]-header[1]/2, header_group[0][2]])
        cube([header[0], header[1], header[2]]);
        
        /* under */
        translate([-under[0]/2-$clr, -under[1]/2-$clr, -under[2]+$fix])
        cube([under[0]+$clr*2, under[1]+$clr*2, under[2]+$clr+$fix]);
        
        /* heatsink */
        translate([-heatsink[0]/2-$clr, -heatsink[1]/2-$clr, -heatsink[2]-under[2]+$fix])
        cube([heatsink[0]+$clr*2, heatsink[1]+$clr*2, heatsink[2]+$clr+$fix]);

        /* heatsink holes */
        translate([0, 0, -under[2]+$fix])
        heatsink(base=heatsink, pocket=heatsink_pocket, outer=(bound[0]-heatsink[0])/2+$fix);
        
        /* cooler */
        translate([0, 0, -heatsink[2]-under[2]+$fix*2]){
            rotate([0, 180, 0])
            cooler([cooler[0]+$clr*2, cooler[1]+$clr*2, cooler[2]+$fix*4]);
            
            /* cooler wire channel */
            translate([cooler[0]/2-9, cooler[0]/2, -7])
            rotate([45, 0, 0])
            cube([5, 7, 3]);
            
            translate([cooler[0]/2-9, cooler[0]/2, -3])
            cube([5, (heatsink[1]-cooler[0])/2+3, 3]);

            translate([cooler[0]/2-9, heatsink[1]/2-$fix, -$fix])
            cube([5, 3, heatsink[2]+$fix*2]);
        }
    }
}

module assembly(
    mode="full"
){
    if(mode == "full" || mode == "%full" || mode == "pure"){
        translate([0, -34, 0])
        power_board();
        
        translate([0, 32, 0])
        logic_board();
        
        translate([0, 0, -5])
        heatsink();
        
        translate([0, 0, -27])
        rotate([0, 180, 0])
        cooler();
    }

    if(mode == "full" || mode == "only"){
        housing();
    }

    if(mode == "%full" || mode == "%only"){
        %housing();
    }
}

$fn=100;
$fix=0.01;
$clr=0.3;

assembly(
    //mode="%full"
    //mode="pure"
    mode="only"
);
