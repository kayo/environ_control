#define _CAT2(a, b) a##b
#define CAT2(a, b) _CAT2(a, b)
#define _CAT3(a, b, c) a##b##c
#define CAT3(a, b, c) _CAT3(a, b, c)
#define _CAT4(a, b, c, d) a##b##c##d
#define CAT4(a, b, c, d) _CAT4(a, b, c, d)

/* Not connected */
#define PNC_PORT NULL
#define PNC_PAD 0

#define PORT(x) CAT2(x, _PORT)
#define PAD(x) CAT2(x, _PAD)
#define MODE(x) CAT2(PAL_MODE_, x)
#define EXT(x) CAT2(EXT_MODE, CAT2(x, _GPIO))
#define EDGE(x) CAT3(EXT_CH_MODE_, x, _EDGE)

#define ALTERNATE_PUSHPULL STM32_ALTERNATE_PUSHPULL
#define ALTERNATE_OPENDRAIN STM32_ALTERNATE_OPENDRAIN

/* Pad Modes:
 * RESET
 * UNCONNECTED
 * INPUT
 * INPUT_PUSHPULL
 * INPUT_PULLDOWN
 * INPUT_ANALOG
 * OUTPUT_PUSHPULL
 * OUTPUT_OPENDRAIN
 * ALTERNATE_PUSHPULL
 * ALTERNATE_OPENDRAIN
 */
