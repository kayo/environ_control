/* 7-seg clock display */
/* segments: */
#define SEG_A PA12
#define SEG_B PB3
#define SEG_C PB5
#define SEG_D PB7
#define SEG_E PB8
#define SEG_F PA15
#define SEG_G PB4
#define SEG_P PB6
/* digits: */
#define SEG_1 PD1
#define SEG_2 PD0
#define SEG_3 PC13
#define SEG_4 PB9

/* Handler encoder with button */
#define ENC_DRV EXTD1
#define ENC_A PB10
#define ENC_B PB11
#define ENC_BTN PB12

/* Zero-cross/phase detector */
#define ZERO_DRV EXTD1
#define ZERO_DT PB13

/* Thermistor sensors */
#define T1_DRV ADCD1
#define T1_IN PA2
#define T1_CH 2
#define T1_RT 10000
#define T1_R1 4700
#define T1_R2 0
#define T2_DRV ADCD1
#define T2_IN PA3
#define T2_CH 3
#define T2_RT 10000
#define T2_R1 4700
#define T2_R2 0
#define T3_DRV ADCD1
#define T3_IN PA0
#define T3_CH 0
#define T3_RT 10000
#define T3_R1 4700
#define T3_R2 0
#define T4_DRV ADCD1
#define T4_IN PA1
#define T4_CH 1
#define T4_RT 10000
#define T4_R1 4700
#define T4_R2 0

/* Heatsink sensor */
#define TS_DRV ADCD1
#define TS_IN PB0
#define TS_CH 8
#define TS_RT 10000
#define TS_R1 4700
#define TS_R2 0

/* Heatsink fan pwm */
#define FAN_DRV PWMD1
#define FAN_CH 4
#define FAN_POL PWM_OUTPUT_ACTIVE_HIGH
#define FAN_OUT PA11

/* Load gates */
#define LOAD_1 PB14
#define LOAD_2 PB15
#define LOAD_3 PA8

/* Radio SPI */
#define RAD_DRV SPID1
#define RAD_CS PA4
#define RAD_CK PA5
#define RAD_DO PA6
#define RAD_DI PA7
#define RAD_EV PB1
#define RAD_ON PB2

/* UART port */
#define UART_DRV UARTD1
#define UART_TX PA9
#define UART_RX PA10

/* Free flash */
#define FLASH_SIZE 64<<10
#define FLASH_FREE 1<<10
